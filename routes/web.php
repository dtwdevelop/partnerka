<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// front routes

//use Illuminate\Routing\Route;

use App\Http\Controllers\DeviceController;
use Illuminate\Support\Facades\Route; 

$routes  = [
    ['route'=> '/'],
    ['route'=>'/users'],
    ['route'=>'/packages'],
    ['route'=>'/package/add'],
    ['route'=>'/package/edit/{id}'],
    ['route'=>'/settings'],
    ['route'=>'/categories'],
    ['route'=>'/dealers'],
    ['route'=>'/logout'],
    ['route'=>'/dashboard'],
    ['route'=>'/notifies'],
    ['route'=>'/generator'],
    ['route'=>'/statistic'],
    ['route'=>'/profile'],
    ['route'=>'/status'],
];



Route::middleware(['api','auth:api'])->group(function ()use($routes) {
    foreach($routes  as $route){
        Route::get($route['route'], function () {
            return view('welcome');
        });
    }
    //category routes
Route::get('/get-categories','CategoryController@get_list');
Route::get('/category-edit','CategoryController@category_edit');
Route::post('/add-channel','CategoryController@add_to_category');
Route::put('/category-update','CategoryController@category_update');
Route::post('/category/add','CategoryController@create');
Route::delete('/category-delete','CategoryController@destroy');
//package routes
Route::get('/get-packages','PackageController@get_packages');
Route::get('/get-package','PackageController@get_package');
Route::post('/package/create','PackageController@create_package');
Route::delete('/delete-package','PackageController@destroy');

Route::get('/list-channels','ChannelController@channels');
Route::get('/channels','ChannelController@index');
Route::get('/playlist','PackageController@generate_playlist');

//client
Route::get('/get-clients','ClientController@clients');
Route::post('/add-client','ClientController@store');
Route::delete('/delete-client','ClientController@destroy');
Route::post('/change-note','ClientController@change_note');
Route::put('/change-status','ClientController@change_status');

//countries
Route::get('countries','CountryController@get_all');

//device
Route::get('/devices','DeviceController@devices');
Route::post('/add-device','DeviceController@store');
//ministra
Route::post('/add-stalker','DeviceController@add_stalker');
Route::get('/delete-stalker','DeviceController@destroy');
Route::get('/change-status-stalker','DeviceController@change_status_stalker');
Route::get('/get-stalker-tariff','DeviceController@get_stalker_tariff');
//portal
Route::get('/get-portal-tariffs','DeviceController@get_portal_tariffs');
Route::post('/add-portal','DeviceController@add_portal');
Route::delete('/delete-portal','DeviceController@delete_portal');
Route::get('/disable-portal','DeviceController@disable_portal');
Route::put('/change-portall-tarrifs','DeviceController@change_portal_tarrifs');

//users
Route::post('/add-user','UserController@store');
Route::get('/get-users','UserController@index');
Route::post('/change-password','UserController@update_password');
Route::delete('/delete-user','UserController@destroy');
Route::post('/token-refresh','UserController@refresh');
Route::get('/notify','UserController@notifies');

Route::get('/add-extend','ClientController@balance_extend');   
//session tokens

});


//auth and exit
Route::post('/auth/login','UserController@login');

Route::get('/login', function () {
    return view('welcome');
})->name('login');

Route::post('/auth/logout','UserController@logout');
Route::get('/debug' , 'DeviceController@get_list_subscriber');

//
Route::post('/get-apikey','ClientController@getKey');
Route::get('/count-lisence','UserController@update_license');
Route::get('/token-session','SettingController@get_token_count');

//playlist
Route::get('/get-countries-by-package','PackageController@get_country_by_package');
Route::delete('/delete-playlist','DeviceController@delete_playlist');
Route::get('/package-urls/{hash}/{type}/{file}','PackageController@generate_playlist');
//token check
Route::get('/auth','DeviceController@token_check');
Route::get('/add-settings','SettingController@settings_save');
Route::get('/stream-test','ChannelController@api_test');
Route::get('/channels-status','ChannelController@channels_status');