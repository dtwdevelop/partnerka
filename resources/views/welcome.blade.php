<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

        <title>Parther system</title>
     
 </head>
    <body>
        <div id="app"></div>
<script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
   <script src="/js/lib/vue-router.js"></script>
   <script src="/js/lib/vuex.js"></script>
   <script src="/js/lib/axios.min.js"></script>
   <script src="/js/lib/validators.min.js"></script>
   <script src="/js/lib/vuelidate.min.js"></script>
 
   <script src="https://unpkg.com/vue-recaptcha@^1/dist/vue-recaptcha.min.js"></script>
   <!-- <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.min.js"></script> -->
   <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script> 
   <script src="/js/store.js"></script>
   <script src="/js/router.js" type="module"></script>
    </body>
</html>
