<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  =[
            'name' => 'admin',
             'email'=>'super@admin.com',
             'password' => bcrypt('demodemo'),
              'role' => 'super admin',
              'note' => 'admin account',
               'is_active' => true,
               'count' => 100
        ];
      $id =  DB::table('users')->insertGetId($user);

        DB::table('balances')->insert([
          'user_id' => $id,
          'balance' => 1000,

        ]);
        //
    }
}
