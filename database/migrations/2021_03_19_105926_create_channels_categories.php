<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('channel_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('channel_id')->references('id')->on('channels');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('channels_categories', function (Blueprint $table) {
            $table->dropForeign('channels_categories_channel_id_foreign');
            $table->dropForeign('channels_categories_category_id_foreign');
        });
        Schema::dropIfExists('channels_categories');
    }
}
