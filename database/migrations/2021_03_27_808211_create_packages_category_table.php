<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('packages_categories', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->unsignedBigInteger('package_id')->nullable();
           $table->unsignedBigInteger('country_id')->nullable();
           $table->foreign('package_id')->references('id')->on('packages');
           $table->foreign('country_id')->references('id')->on('countries');
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages_categories', function (Blueprint $table) {
            $table->dropForeign('packages_categories_package_id_foreign');
            $table->dropForeign('packages_categories_country_id_foreign');
            
        });
        
        Schema::dropIfExists('packages_categories');
    }
}
