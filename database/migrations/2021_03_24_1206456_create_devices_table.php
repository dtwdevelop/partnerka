<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('package_id')->nullable();
            $table->string('name')->nullable();
            $table->string('password')->nullable();
            $table->enum('type', ['playlist', 'ministra','portal']);
            $table->string('token');
            $table->boolean('status');
            $table->timestamps();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropForeign('devices_client_id_foreign');
            $table->dropForeign('devices_package_id_foreign');
           
         });
        Schema::drop('devices');
    }
}
