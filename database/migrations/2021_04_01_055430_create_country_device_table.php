<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_device', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('device_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('country_device', function (Blueprint $table) {
            $table->dropForeign('country_device_country_id_foreign');
            $table->dropForeign('country_device_device_id_foreign');
           
           
            
        });
        Schema::dropIfExists('country_device');
    }
}
