<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('is_trial');
            $table->boolean('is_normal');
            $table->boolean('is_playlist');
            $table->boolean('is_ministra');
            $table->boolean('is_iptvportal');
            $table->smallInteger('max_connection');
            $table->enum('type', ['hours', 'days','months' ,'years']);
            $table->smallInteger('total_period');
            $table->date('expire');
            $table->float('credit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('packages');
    }
}
