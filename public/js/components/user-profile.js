var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer,decimal } = window.validators
export default{
    name : 'Profile',
    mixins: [validationMixin],
    template: `
    <div>
  <v-form dense ref="form" >
  <v-alert v-if="status" dense text type="success">
 Password updated
</v-alert>

   <v-row>
   <v-col sm="5">
  <v-text-field v-model="password" :append-icon="show1 ? 'mdi-eye' : 'mdi-eye-off'" :type="show1 ? 'text' : 'password'"
  label="password" hint="At least 8 characters" @click:append="show1 = !show1" :error-messages="passwordErrors" required @input="$v.password.$touch()" @blur="$v.password.$touch()"></v-text-field>
  
  <v-text-field v-model="password_repeat" :append-icon="show2 ? 'mdi-eye' : 'mdi-eye-off'" :type="show2 ? 'text' : 'password'"
  label="password repeat" hint="At least 8 characters" @click:append="show2 = !show2" :error-messages="password_repeatErrors" required @input="$v.password_repeat.$touch()" @blur="$v.password_repeat.$touch()"></v-text-field>
  
  
</v-col>
  
</v-row>
 <v-row>

 <v-col><v-btn small @click="passwordUpdate" :disabled="$v.$invalid">update</v-btn></v-col>
  </v-row>
    
    </v-form>
   
  </div>`,
    data: () => ({
      status : false,
      password:  '',
      password_repeat :  '',
      show1: false,
      show2: false,
        }),

    validations:{
     password:{required ,minLength:minLength(6)},
     password_repeat :{ required , sameAsPassword : sameAs('password')}
    },
    computed:{
       

      account: function(){
        return this.$store.state.account
      
      },
      passwordErrors() {
        const errors = []
        if (!this.$v.password.$dirty) return errors
        !this.$v.password.required && errors.push('password required')
        !this.$v.password.minLength && errors.push('password must be more than 6 symbols')
        
        return errors
      },
      password_repeatErrors() {
        const errors = []
        if (!this.$v.password_repeat.$dirty) return errors
        !this.$v.password_repeat.required && errors.push('password repeat required')
        !this.$v.password_repeat.sameAsPassword && errors.push('Passwords must be identical.')
        
        return errors
      },

      

     

    },
    mounted(){
    
    
    },
    methods:{
        passwordUpdate(){
            const $this = this
            this.$v.$touch();
            if(!this.$v.$invalid){
                  console.log(this.account.user.id)
                  this.$store.dispatch('updatePassword',{ user_id: this.account.user.id, password:this.password}).then(()=>{
                   $this.status  = true
                   
                  })
                 
            }
          
          }

    }
}