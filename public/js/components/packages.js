export default {
    name: 'Packages',
    template :`<div>
    <v-card>
    <v-card-title>
      <v-text-field  v-model="search" append-icon="mdi-magnify" label="Search"
        single-line
        hide-details
      ></v-text-field>
    </v-card-title>
    <v-btn tile color=" darken-1" to="/package/add" class="text--white" small>Add <v-icon small>mdi-plus</v-icon></v-btn>
    
    <v-data-table v-if="loader" :dense="true" :headers="headers" :items="packages" :search="search">

<template v-slot:item.is_trial="{ item }">

<v-icon small v-if="item.is_trial">mdi-check</v-icon>
<v-icon small v-else>mdi-close</v-icon>
</template>

<template v-slot:item.is_normal="{ item }">
<v-icon small v-if="item.is_normal">mdi-check</v-icon>
<v-icon small v-else>mdi-close</v-icon>
</template>

<template v-slot:item.normal_info="{ item }">
<span v-show="item.is_normal">
{{item.credit}}.00
</span>
</template>

<template v-slot:item.trial_info="{ item }">
<span v-show="item.is_trial">
{{item.credit}}.00
</span>
</template>

<template v-slot:item.is_playlist="{ item }">
<v-icon small v-if="item.is_playlist">mdi-check</v-icon>
<v-icon small v-else>mdi-close</v-icon>
</template>

<template v-slot:item.is_ministra="{ item }">
<v-icon small v-if="item.is_ministra">mdi-check</v-icon>
<v-icon small v-else>mdi-close</v-icon>
</template>

<template v-slot:item.is_iptvportal="{ item }">
<v-icon small v-if="item.is_iptvportal">mdi-check</v-icon>
<v-icon small v-else>mdi-close</v-icon>
</template>

<template v-slot:item.options="{ item }">
<v-btn small text :to="'/package/edit/'+item.id"><v-icon small  >mdi-pencil</v-icon></v-btn>
<v-btn small text @click="confirmDelete(item)"><v-icon small>mdi-delete</v-icon></v-btn>
<v-dialog  v-model="confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title class="headline">
   Delete package ?
  </v-card-title>
  <v-card-text></v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="confirm = false" >
      Disagree
    </v-btn>
    <v-btn  color="green darken-1" text @click="deletePackage(item.id)" >
      Agree
    </v-btn>
  </v-card-actions>
</v-card>
</v-dialog>
</template>

    </v-data-table>
    <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
    <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
  </v-sheet>
  </v-card>
    </div>`,
    data:()=>({
         search: '',
         confirm : '',
         loader : false,
         del_id :null,
        
         headers:[
          {text:'id', value:"id"},
             {text:'Name', value:"name"},
             {text: 'Is trial' ,value:"is_trial"},
             {text: 'Is normal' ,value:"is_normal"},
             {text: 'Trial info' ,value:"trial_info"},
             {text: 'Normal info' ,value:"normal_info"},
             {text: 'Playlist' ,value:"is_playlist"},
             {text: 'Ministra' ,value:"is_ministra"},
             {text: 'Iptv portal' ,value:"is_iptvportal"},
             {text: 'Options' ,value:"options"},
            ],
        
    }),


    computed :{
      packages: function(){
        return this.$store.state.packages
      }
 },

    created(){
      const $this = this
      this.$store.dispatch('getPackages').then(()=>{$this.loader =true});

    },
    methods :{
      confirmDelete(item){
        this.confirm = true
        this.del_id  = item.id
     },
      deletePackage(id){
        this.$store.dispatch('deletePackage',{id:this.del_id})
        this.confirm =false
      }

    }
}