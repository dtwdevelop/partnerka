export default {
    name : 'StatusChannels',
   
    template: `
      <div >
      <v-spacer></v-spacer>
    <h3>Channels alive status</h3>
      <v-text-field v-model="search" label="search" single-line hide-details ></v-text-field>
      <v-data-table :dense="true" :headers="headers" :items="channels" :search="search" >
      <template v-slot:item.status="{ item }">
      <v-icon color="red" dark>mdi-playlist-remove </v-icon>
    </template>
     </v-data-table>
       </div>
       `,
    data: () => ({
     search : '' ,
  
      headers: [
        { text: 'name', value :'name' },
        { text: 'title', value :'title' },
        { text: 'status', value: 'status' },
      
   ],
    
    }),
  
  
    computed: {
      channels: function () {
        return this.$store.state.channels_status
      },
      
     
    },
  
    mounted() {
     this.$store.dispatch("getChannelsStatus")
    },
  
    methods: {
     
    }
  }