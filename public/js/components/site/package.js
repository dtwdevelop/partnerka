

const Package = Vue.component('Package', {

  template: `
    <div >
   
    <v-dialog v-model="dialog" max-width="290" >
        <v-card >
          <v-card-title class="headline">{{$t('message.user_package_title')}}</v-card-title>
         <v-card-text>
         {{$t('message.user_package_text')}}
          </v-card-text>
         <v-card-actions>
            <v-spacer></v-spacer>
  
            <v-btn color="green darken-1"  text  @click="dialog = false" >
            {{$t('message.user_not_agree')}}
            </v-btn>
  
            <v-btn color="green darken-1" text  @click="BuyPackage(select,account.user.user_id)" >
            {{$t('message.user_buy')}}
            </v-btn>
          </v-card-actions>
        </v-card>
      </v-dialog>

      <v-dialog v-model="dialog2" max-width="290" >
      <v-card>
        <v-card-title class="headline">{{$t('message.user_not_balance_title')}}</v-card-title>
       <v-card-text>
       {{$t('message.user_not_balance_text')}}   <v-btn link to="billing/payment" text>{{$t('message.user_add_balance')}}</v-btn>
        </v-card-text>
       <v-card-actions>
          <v-spacer></v-spacer>

          <v-btn color="green darken-1"  text  @click="dialog2 = false" >
          {{$t('message.close')}}
          </v-btn>

        </v-card-actions>
      </v-card>
    </v-dialog>

    <v-container fluid >
<v-row>
<v-col v-for="pack in packages" :key="pack.id">
    <v-card  >
    <v-card-title>{{$t('message.user_package')}} {{pack.title}}</v-card-title>
    <v-card-subtitle > £ {{pack.price}}  </v-card-subtitle class="pb-0">
    <v-card-text class="text--primary">
   
   <v-container fluid>
    <v-radio-group  v-model="select" :mandatory="false" >
     <v-radio  :label="pack.title" :value="pack.id">
    </v-radio>
     </v-radio-group>
    </v-container>
    </v-card-text>

      <v-card-actions>
   <v-btn color="primary" @click="dialog=true"> {{$t('message.user_buy')}} </v-btn>
   </v-card-actions>
   </v-card>
   </v-col>
   </v-row>
   </v-container >
  </div>
   
  `,
  data: () => ({
    select: "",
    agree: true,
    dialog: false,
    dialog2: false

  }),

 async mounted() {
   this.$store.dispatch("Account", { id: this.$store.state.user.ID })
    this.$store.dispatch("getMPackage")
   
  },

  computed: {
    packages: function () {
      return this.$store.state.mpackages
    },

    account: function () {
      return this.$store.state.account
    },
    balance: function () {
      return this.$store.state.balance
    },
},
  methods: {
    BuyPackage(id,user_id) {
     
     this.packages.forEach(pack => {
          if (pack.id == id) {
          
          
            if (parseFloat(this.balance) >= parseFloat(pack.price)) {
              const balance = this.balance - pack.price
              this.$store.commit('change_balance', { price: balance })
              this.$store.dispatch("updateBalance",{balance : balance ,id:this.account.user.id})
              this.$store.dispatch('sendEmailPackageComplete',{id:this.account.user.id ,suma:pack.price, email:this.account.user.email, name:this.account.user.user_login ,pack:pack.title})
              this.$store.dispatch("UpdateStalkerPackage",{id: this.account.user.id, package_id:id })
              this.$store.dispatch("UpdateUserPackage", { user_id: user_id, package_id:id })
              this.$store.dispatch("UpdateUserStatus", { user_id: user_id, status: "true" })
            
            
              this.$emit('updateme', "test")
              this.dialog = false
            
            //  
            }
            else {
              this.dialog = false
              this.dialog2 = true
             
            }
            
          }
        });

      

    },

  },
})

export { Package }