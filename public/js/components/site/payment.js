import { PaypalComponent} from './paytype/paypal.js'
import { StripeComponent} from './paytype/stripe.js'
import { CheckoutComponent} from './paytype/2checkout.js'
import { CoinbaseComponent} from './paytype/coinbase.js'
const Payment = Vue.component('Payment', {
  components : {PaypalComponent ,StripeComponent, CheckoutComponent ,CoinbaseComponent},
  template: `
    <div >
    <ip-menu></ip-menu>
  
    <v-stepper v-model="e1">
    <v-stepper-header>
    <v-stepper-step :complete="e1 > 1" step="1">{{$t('message.user_payment')}} </v-stepper-step>
    <v-divider></v-divider>
   <v-stepper-step :complete="e1 > 2" step="2">{{$t('message.user_method_choice')}} {{paytype}} </v-stepper-step>
    <v-divider></v-divider>
   <v-stepper-step :complete="e1 >= 3" step="3">{{$t('message.user_payment_finished')}}</v-stepper-step>
    </v-stepper-header>
   <v-stepper-items>

   <v-stepper-content step="1">
        <v-card class="mb-12" height="200px">
        <v-container fluid>
     <v-text-field @change="onChangeTotal()"  clearable prefix="£"  v-model="sum" label="Сума" single-line solo ></v-text-field>
     <v-radio-group  v-model="paytype" :mandatory="false" >
     
     <v-radio  v-if="options.paypal_status"   :label="$t('message.payment_paypal')" value="paypal"></v-radio>
     <v-radio  v-if="options.stripe_status"  :label="$t('message.payment_card')" value="stripe"></v-radio>
     <v-radio  v-if="options.checkout2_status"  :label="$t('message.payment_checkout2')" value="2checkout"></v-radio>
     <v-radio  v-if="options.coinbase_status"  :label="$t('message.payment_coinbase')" value="coinbase"></v-radio>

     </v-radio-group>
    </v-container>
    </v-card>
   <v-btn color="primary" :disabled="sum == ''?true:false " @click="PaymentType()">
   {{$t('message.but_continue')}}
        </v-btn>
  </v-stepper-content>


  <v-stepper-content step="2">
       
      <v-form>
        <v-container fluid>
        <v-row>
        <v-col>

        <div v-if="paytype == 'paypal'">
        <paypal-component @finished="Continue" append-icon="fab fa-paypal" :account="account" :suma="sum">   </paypal-component>
        </div>
        <div v-if="paytype == 'stripe'" >
        <stripe-component @finished="Continue" append-icon="fab fa-stripe"  :account="account" :suma="sum"  :keystripe="key">   </stripe-component>
         </div>
        <div v-if="paytype == '2checkout'">
        <checkout-component @finished="Continue" :account="account" :suma="sum">   </checkout-component>
        </div>
        <div v-if="paytype == 'coinbase'">
        <coinbase-component @finished="Continue" :account="account" :suma="sum">   </coinbase-component>
        </div>
        </v-col>
        </v-row>
        </v-container>
        </v-form>
        <v-row>
        <v-col>
        <v-btn text @click="goBack(1)"> {{$t('message.back')}}</v-btn>
        </v-col>
        </v-row>
       </v-stepper-content>

      


   <v-stepper-content step="3">
        <v-card class="mb-12" height="200px" >
        {{$t('message.user_payment_succeful_text')}} £ {{ sum }} 
        <v-divider></v-divider>
        </v-card>
       </v-stepper-content>
    </v-stepper-items>
  </v-stepper>
  </div>
   
  `,
  data: () => ({
   
    e1: 0,
    paytype: "",
    select: "",
    sum: "",
    show2: false,
    stripe : null,
    card : null,
    show3: false,
    error : false,
    message: ''

  }),

  created(){
    
},

 async mounted() {
   //180
    this.$store.dispatch("loadPackages")
    this.$store.dispatch("Account", { id: this.$store.state.user.ID })
    //this.$store.dispatch("Account", { id: 76 })
    this.stripe = Stripe('pk_test_PXLxlVsJJLX4IxZG5JEOU4o600ABEKtpVc');
    this.$store.dispatch("GetSetting").then(function(){
    
    });

  
},

computed: {
    packages: function () {
      return this.$store.state.packages
    },
   
    account: function () {
      return this.$store.state.account
    },

    options: function () {
      return this.$store.state.options
    },

   
    key:function(){
      return this.$store.state.key
     }
  },
  methods: {
    async onChangeTotal(){
      this.$store.dispatch("getKey",{ data: this.sum})
    },

 

  goBack(step) {
      this.e1 = step
    },

    PaymentType() {
      this.e1 = 2
      const $this = this
     },

     Continue(ev){
      if(ev == 'true'){
        this.e1 = 3
      }
      else{

      }
    
     }

  }
})
export { Payment }