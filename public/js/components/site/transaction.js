const Transaction = Vue.component('Transaction', {

  template: `
    <div >
    <ip-menu></ip-menu>
    <v-data-table :headers="headers" dense :items="payments" class="elevation-3" :items-per-page="20" >
    <template v-slot:item.payment="{ item }">
 -{{item.payment}} &pound;
    </template>
    </v-data-table>
   </div>
   
  `,
  data: () => ({
    headers: [
    
      {
        text: 'payed',
        sortable: false,
        value: 'payment',
      },
      {
        text: 'type',
        sortable: true,
        value: 'type',
      },
      {
        text: 'created',
        sortable: true,
        value: 'time',
      },
    ],

    payments: [
      {
      
        payment: 20,
        type :  'paypal'
      },
      {
      
        payment: 10,
        type :  'stripe'
      }
    ]
  
    }),

 async mounted() {
  
  },

  computed: {
  
},
  methods: {
  
  },


})

export { Transaction }