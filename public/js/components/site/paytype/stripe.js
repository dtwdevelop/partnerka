const StripeComponent = Vue.component("StripeComponent", {
    props: {
      account : Object,
      suma : String,
      keystripe : String,
     
     },
    template: `
      <div>
     <v-card class="mx-auto"max-width="500">
     <v-card-title>
     <v-alert type="error" v-if="show2">
      {{$t('message.user_payment_error')}}
    </v-alert>
    <v-alert type="success" v-if="show3">
    {{$t('message.user_payment_succeful')}}
    </v-alert>
    </v-card-title>
      <v-card-text>
      <v-text-field v-model="account.user.user_login"  :label="$t('message.name')"  ></v-text-field>
      <div  id="stripe" >
      <!-- A Stripe Element will be inserted here. -->
      </div>
      <v-alert dense type="error" v-if="error">{{message}}</v-alert>
      </v-card-text>
      <v-card-actions>
      <v-btn small @click="payStripe(suma)">{{$t('message.user_buy')}}</v-btn>
      </v-card-actions>
    </v-card>
        </div>
      `,
    data: () => ({
    
      show2: false,
      stripe : null,
      card : null,
      show3: false,
      error : false,
      message: '',
      name : '',
  
    }),
    async mounted() {
      // //180
      //  this.$store.dispatch("loadPackages")
      //  this.$store.dispatch("Account", { id: this.$store.state.user.ID })
      //  this.stripe = Stripe('pk_test_PXLxlVsJJLX4IxZG5JEOU4o600ABEKtpVc');
      this.stripe = Stripe('pk_test_PXLxlVsJJLX4IxZG5JEOU4o600ABEKtpVc');
   
      const elements = this.stripe.elements();
      //const prButton = elements.create('card', { paymentRequest: paymentRequest });
      const elementsStyles = {
       base: {
         color: "#004ABB",
         iconColor: "#004ABB",
         fontFamily: "'Mukta', Helvetica, sans-serif",
         fontWeight: "600",
         fontSmoothing: "antialiased",
         fontSize: "16px",
         "::placeholder": {
           color: "#6099EE",
           textTransform: "uppercase",
         },
       },
       invalid: {
         color: "#ff5252",
         iconColor: "#ff5252",
       },
     };
      this.card = elements.create('card',{ hidePostalCode: true,  style: elementsStyles  } );
     // this.card = elements.create('card',{   style: elementsStyles  } );
      this.card.mount('#stripe')
      const $this = this
      this.card.on('change', function(event) {
      
       if (event.error) {
          $this.message = event.error.message
          $this.error = true
        } else {
         $this.message = ''
         $this.error = false
        }
      });
   },
   
    computed:{
      balance: function () {
        return this.$store.state.balance
      },
     
      } ,
     
    methods: {
      async  payStripe(credit){
   
        const $this = this
        const total =  parseInt(credit)
        $this.stripe.confirmCardPayment(this.keystripe, {
         payment_method: {
           card: $this.card,
           billing_details: {
             name: $this.account.user.email
           }
         }
       }).then(function(result) {
         if (result.error) {
          $this.show2 = true
          $this.show3 = false
           console.log(result.error.message);
         } else {
           // The payment has been processed!
           if (result.paymentIntent.status === 'succeeded') {
             $this.show2 = false
             $this.show3 = true
             const balance = parseFloat($this.balance) + parseFloat($this.suma)
             $this.$store.commit('change_balance', { price: balance })
             $this.$store.dispatch("updateBalance",{balance : parseFloat(balance) ,id:$this.account.user.id})
             $this.$store.dispatch('sendBuyCreditComplete',{suma: parseFloat($this.suma), id:$this.account.user.id, email:$this.account.user.email, name:$this.account.user.user_login,tpay:'stripe' })
             setTimeout(()=>{  $this.$emit('finished','true') },500)
             }
         }
       });
   
   },
    }
  })
  export { StripeComponent }