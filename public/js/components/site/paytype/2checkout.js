const CheckoutComponent = Vue.component("CheckoutComponent", {
    props: {
      suma : String,
      account :Object
     },
    template: `
      <div>
       <v-card class="mx-auto"max-width="500">
     <v-card-title>
     <v-alert type="error" v-if="show2">
      {{$t('message.user_payment_error')}}
    </v-alert>
    <v-alert type="success" v-if="show3">
    {{$t('message.user_payment_succeful')}}
    </v-alert>
    </v-card-title>
     <v-card-text>
     <v-text-field v-model="account.user.user_login"  hint="John Doe" :label="$t('message.name')"  ></v-text-field>
     <div id="card-element"></div>
    </v-card-text>
     <v-card-actions>
     <v-btn small  @click="PayOrder()" >{{$t('message.user_buy')}}</v-btn>
     </v-card-actions>
   </v-card>
     
     
    </div>
        
    `,
    data: () => ({
       
        show2: false,
        show3: false,
        error : false,
        message: '',
        name : '',
        component : null,
        client : null,
  
    }),

    created(){

    },
    mounted(){

  this.client = new TwoPayClient('250467475416');
  // Create the component that will hold the card fields.
  this.component = this.client.components.create('card');
  
  // Mount the card fields component in the desired HTML tag. This is where the iframe will be located.
  this.component.mount('#card-element');

  // Handle form submission.
  
      

    },
   
    computed:{
     
      balance: function () {
        return this.$store.state.balance
      },
     
      } ,
     
    methods: {
      PayOrder(){
      
        console.log(this.account.user.user_login)
       // Extract the Name field value
          const billingDetails = {
            name: this.account.user.user_login
          };
      
          // Call the generate method using the component as the first parameter
          // and the billing details as the second one
          const $this = this
          this.client.tokens.generate(this.component, billingDetails).then((response) => {
            console.log(response.token);
            $this.$store.dispatch('sendToken',{token:response.token ,suma:$this.suma}).then((data)=>{
              console.log(data)
               const balance = parseFloat($this.balance) + parseFloat($this.suma)
             $this.$store.commit('change_balance', { price: balance })
             $this.$store.dispatch("updateBalance",{balance : parseFloat(balance) ,id:$this.account.user.id})
             $this.$store.dispatch('sendBuyCreditComplete',{suma: parseFloat($this.suma), id:$this.account.user.id, email:$this.account.user.email, name:$this.account.user.user_login,tpay:'2checkout' })
             $this.$emit('finished','true')
            })
            
           
          }).catch((error) => {
            console.error(error);
          });
        
      }

    }
  })
  export {CheckoutComponent  }