var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer } = window.validators
export default{
    name : 'Client',
    mixins: [validationMixin],
    template: `
    <div>
  <v-form dense  ref="form">
    <v-row>
   <v-col sm="4"> <v-text-field v-model="client.name" label="Name" :error-messages="nameErrors" required @input="$v.client.name.$touch()" @blur="$v.client.name.$touch()" ></v-text-field></v-col>
   <v-col sm="4"> <v-text-field v-model="client.email" label="Email"  :error-messages="emailErrors" required @input="$v.client.email.$touch()" @blur="$v.client.email.$touch()" ></v-text-field></v-col>
 </v-row>
   <v-row>
   <v-col sm="6"> <v-textarea v-model="client.note" label="Note" :error-messages="descriptionErrors" required @input="$v.client.note.$touch()" @blur="$v.client.note.$touch()"></v-textarea></v-col>
   </v-row>
 <v-row>
   <v-col><v-btn small @click="createClient">save</v-btn></v-col>
    </v-row>
    </v-form>
  </div>`,
    data: () => ({
        menu: false,
        date: new Date().toISOString().substr(0, 10),
      

        client:{
            name : '',
            email : '',
            note : '',
           }

    }),
    validations:{
      client:{
        name:{required,minLength:minLength(4)},
        email:{required ,email,minLength:minLength(4)},
        note:{required ,minLength:minLength(4)},

      }
    },
    computed:{
      nameErrors() {
        const errors = []
        if (!this.$v.client.name.$dirty) return errors
        !this.$v.client.name.required && errors.push('Name required')
        !this.$v.client.name.minLength && errors.push('Description must be more than 4 symbols')
        return errors
      },

      descriptionErrors() {
        const errors = []
        if (!this.$v.client.note.$dirty) return errors
        !this.$v.client.note.required && errors.push('Description required')
        !this.$v.client.note.minLength && errors.push('Description must be more than 4 symbols')
        return errors
      },
      emailErrors() {
        const errors = []
        if (!this.$v.client.email.$dirty) return errors
        !this.$v.client.email.required && errors.push('Description required')
        !this.$v.client.email.minLength && errors.push('Description must be more than 4 symbols')
        !this.$v.client.email.email && errors.push('email format werong')
        return errors
      },

    },
    mounted(){
      

    },
    methods:{
      createClient(){
        const $this  = this
        this.$v.$touch();
        if(!this.$v.$invalid){
             
              this.$store.dispatch('addClient',{client:this.client}).then(()=>{
                $this.$refs.form.reset()
                $this.$refs.form.resetValidation()
                $this.$emit('clicked')
              })
           
            
             
        }
      
      }

    }
}