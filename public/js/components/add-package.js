var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer } = window.validators
export default{
    name :'AddPackage',
    mixins: [validationMixin],
    template:`
    <div>
    <v-container>
   <v-card>
   <v-card-title>Add package</v-card-title>
   <v-card-text>
   <v-form  >
   <v-row>
   <v-col sm="3">
   <v-text-field label="Package name" v-model="package.name" :error-messages="nameErrors" required @input="$v.package.name.$touch()"@blur="$v.package.name.$touch()" ></v-text-field>
   </v-col>

   <v-col> 
   <v-switch v-model="package.is_normal" label="Status" @change="changeStatus()"  ></v-switch>
   
   </v-col>
   <span v-if="package.is_normal">
   <v-col>
   <p>Normal settings</p> <v-text-field label="Normal duration" v-model="package.duration_time" :error-messages="durationTimeErrors" required @input="$v.package.duration_time.$touch()" @blur="$v.package.duration_time.$touch()" ></v-text-field>
   </v-col>
   <v-col> 
    <v-select label="Duration in"  v-model="package.duration" :items="duration" item-value="id"  dense item-text="val" return-object>
    </v-select>
    </v-col>
</span>


   <span  v-else>
   <v-col>  <v-text-field label="Trial duration" v-model="package.duration_time" :error-messages="durationTimeErrors" required @input="$v.package.duration_time.$touch()" @blur="$v.package.duration_time.$touch()" ></v-text-field></v-col>

   <v-col>  <v-select :items="duration" v-model="package.duration" item-value="id" dense item-text="val" label="Duration in" return-object></v-select></v-col>
   </span>

   
  
   <v-col>  
  
   <v-text-field v-model="package.credit" label="Credit cost" :error-messages="creditErrors"  required @input="$v.package.credit.$touch()" @blur="$v.package.credit.$touch()" ></v-text-field>
   </v-col>
   </v-row>

  <v-row>
   <v-col sm="3"> 
   <p>Reseller can create device</p> 
   <v-checkbox v-model="package.is_playlist"  label="Playlist" ></v-checkbox>
   </v-col>
   <v-col> 
   <v-checkbox v-model="package.is_ministra"  label="Ministra" ></v-checkbox>
   </v-col>
   <v-col> 
   <v-checkbox v-model="package.is_iptvportal"  label="Iptv portal" ></v-checkbox>
   </v-col>
  </v-row>
  <v-row>

  <v-col sm="3">
   <v-text-field v-model="package.max_connection" label="Max connection" :error-messages="maxConnectionErrors"  required @input="$v.package.max_connection.$touch()" @blur="$v.package.max_connection.$touch()"  ></v-text-field> </v-col>
 </v-row>

   <v-row>
   <v-col sm="5">
   <v-autocomplete v-show="package.is_playlist" v-model="package.categories" item-value="id" dense item-text="title" :items="categories"  label="Category"   multiple return-object>
   </v-autocomplete>
   </v-col>
  </v-row>
  <v-row>
  <v-col>

  <v-btn  tile to="/packages" small color="orange">back</v-btn>
  <v-btn class="ml-4" tile small color="green" @click="createCategory">save</v-btn>
 
  </v-col>
  
  </v-row>
   
   </v-form>
   </v-card-text>
   </v-card>
   </v-container>
    </div>`,
    data: ()=>({
      status: true,
      diasabled:true,
      duration: [{id:1 , val:"years"},{id:2 ,val:"months"}, {id:3 ,val:"days"} ,{id:4, val:"hours"}],
        package:{
           name :  '',
          
           is_normal : false,
           is_playlist : false,
           is_ministra : false,
           is_iptvportal : false,
           duration : null,
           duration_time: '',
           credit : 0,
           max_connection : 3,
           categories : [],
           type : false,
          
        },
       
    }),
    validations: {
       package: {
        name: {
            required,
            minLength: minLength(4)
          },
          credit:{
              required,
              integer,
          },
          duration_time:{
            required,
            integer
         },
         max_connection:{
            required,
            integer,
         }
      

       }
      
    
      },
    computed:{
        categories: function(){
           return this.$store.state.countries
        },

        nameErrors() {
            const errors = []
            if (!this.$v.package.name.$dirty) return errors
            !this.$v.package.name.minLength && errors.push('Name must more than 4 symbol')
            !this.$v.package.name.required && errors.push('Name required')
            return errors
          },

          creditErrors() {
            const errors = []
            if (!this.$v.package.credit.$dirty) return errors
            !this.$v.package.credit.required && errors.push('Credit required')
            !this.$v.package.credit.integer && errors.push('Credit must be integer')
            return errors
          },

          maxConnectionErrors() {
            const errors = []
            if (!this.$v.package.max_connection.$dirty) return errors
            !this.$v.package.max_connection.required && errors.push('Credit required')
            !this.$v.package.max_connection.integer && errors.push('Credit must be integer')
            return errors
          },

          durationTimeErrors() {
            const errors = []
            if (!this.$v.package.duration_time.$dirty) return errors
            !this.$v.package.duration_time.required && errors.push('duration required')
            !this.$v.package.duration_time.integer && errors.push('Credit must be integer')
            return errors
          },

    },
    created(){
        this.$store.dispatch('getCountries')
        if(this.$route.params){
         const $this = this
          this.$store.dispatch("getPackageEdit",{id:this.$route.params.id}).then((data)=>{
           $this.package  = data
          })
        }
      
      
    },
    
    methods:{
        createCategory(){
            const $this = this
            this.$v.$touch();
            if(!this.$v.$invalid){
               
                 this.$store.dispatch("addPackage",{package:this.package}).then(()=>$this.$router.push('/packages'))
            }

         },

         changeStatus(){
           this.package.is_normal  = this.package.is_normal
           this.package.is_trial = !this.package.is_normal
         }
        
        

    }
}
