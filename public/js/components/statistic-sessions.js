export default {
    name : 'Statistic',
   
    template: `
      <div >
      <v-spacer></v-spacer>
      <v-switch v-model="version" flat label="api v1 or api v3" @click="onChangeVersion()"></v-switch>
      <v-text-field v-model="search" label="search" single-line hide-details ></v-text-field>
      <v-data-table :dense="true" :headers="headers" :items="sessions" :search="search" >
     
      
      </v-data-table>
       </div>
       `,
    data: () => ({
     search : '' ,
     version : false,
      headers: [
        { text: 'token', value :'token' },
        { text: 'total', value: 'total' },
        { text: 'agent', value: 'agent' },
        { text: 'user_id', value: 'user_id' },
        { text: 'ip', value: 'ip' },
        { text: 'country', value: 'country' },
      
  
     ],
    
    }),
  
  
    computed: {
      sessions: function () {
        return this.$store.state.stattic_sessions
      },
      
     
    },
  
    mounted() {
     this.$store.dispatch("getSessions",{version:false})
    },
  
    methods: {
      onChangeVersion(){
        
        this.$store.dispatch("getSessions",{version:this.version})
      }
    }
  }