export default {
    name : 'Notify',
   
    template: `
      <div >
      <v-data-table :dense="true" :headers="headers" :items="notifies"  >
      <template v-slot:item.action="{ item }">
<v-dialog  v-model="confirm" persistent max-width="300" :retain-focus="false">
    <v-card>

  <v-card-title  class="headline">
   Delete client ?
  </v-card-title>
  
  <v-card-text>
  
  </v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="confirm = false" >
      Disagree
    </v-btn>
    <v-btn  color="green darken-1" text @click="deleteClient(item.data.client)" >
      Agree
    </v-btn>

  </v-card-actions>
</v-card>

</v-dialog>
<v-btn small  text @click="confirmDelete(item)"><v-icon>mdi-delete</v-icon></v-btn>

</template>
      
      </v-data-table>
       </div>
       `,
    data: () => ({
      notify : [],
      confirm : false,
      del_id: null,
      headers: [
        { text: 'id', value :'id' },
        { text: 'dealer', value: 'data.user_id' },
        { text: 'client id', value: 'data.client' },
        { text: 'type', value: 'data.status' },
        { text: 'actions', value: 'action' },
      
  
     ],
    
    }),
  
  
    computed: {
      notifies: function () {
        return this.$store.state.notifies
      },
      account : function (){
          return this.$store.account.user
      }
     
    },
  
    mounted() {
     
    },
  
    methods: {
     deleteClient(id){
        this.$store.dispatch('destroyClient',{id:this.del_id  })
        this.confirm = false

     },
     confirmDelete(item){
         this.confirm =  true
         this.del_id  = item.data.client
     },

     markAsRead(item){

     }

  
    }
  }
  