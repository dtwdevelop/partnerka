const addDealer = ()=>import('./add-dealer.js')
export default {
  name: "Dealer",
  components :{addDealer},
  template: `
  
    <div >
   <v-dialog v-model="dialog" width="600">
   <v-card>
    <v-card-title>Add dealer</v-card-title>
    <v-card-text>
   <add-dealer @clicked="closeDialog"></add-dealer>
    </v-card-text>
    <v-card-actions>
    <v-btn color="blue darken-1" text @click="dialog = false">
    Close
  </v-btn>
    </v-card-actions>
    </v-card>
    </v-dialog>


    <v-card>
    <v-card-title>
      <v-text-field  v-model="search" append-icon="mdi-magnify" label="Search" single-line hide-details></v-text-field>
    </v-card-title>
    <v-btn small @click="dialog =!dialog" color=" lighten-2" dark >add <v-icon small>mdi-plus</v-icon></v-btn>
    <v-data-table :dense="true" :headers="headers" :items="dealers" :search="search" >
   

<template v-slot:item.is_active="{ item }">
<v-switch v-model="item.is_active" ></v-switch>
</template>



<template v-slot:item.action="{ item }">
<v-btn small v-if="item.role != 'superadmin'" text @click="deleteUser(item.id)"><v-icon>mdi-delete</v-icon></v-btn>

</template>
    </v-data-table>
   
  </v-card>
  <v-snackbar v-model="snackbar">
  {{ text }}

  <template v-slot:action="{ attrs }">
    <v-btn color="pink" text v-bind="attrs"  @click="snackbar = false">
      Close
    </v-btn>
  </template>
</v-snackbar>
   </div>
     `,
  data: () => ({
    dialog : false,
    search : '',
    snackbar : false,
    text : 'User created',
   
  
    client: null,
    headers: [
      {
        text: 'id',
        value :'id'
      
      },
      { text: 'name', value: 'name' },
      { text: 'email', value: 'email' },
       {text: 'role', value :'role'},
      { text: 'status', value: 'is_active' },
    
      { text: 'actions', value: 'action' },
      

     
    ],
  }),

  computed:{
   dealers : function(){
     return this.$store.state.users
   }

  },
    
  cretaed(){
   
  },
    
    mounted() {
      this.$store.dispatch('getUsers')
     
   },

  
 

  methods: {
    deleteUser(id){
      this.$store.dispatch('destroyUser',{id:id})
     
    },

    closeDialog(event){
      this.dialog = false
      this.snackbar = true
    }

   

    
   
}

}