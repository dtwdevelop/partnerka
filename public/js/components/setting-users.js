var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer,decimal } = window.validators
export default {
    name: 'UserSettings',
    mixins: [validationMixin],
    props: {
      
        client :Object
       },
    template: `<div>
    <v-dialog  v-model="credit_confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title class="headline">
  No credit !
  </v-card-title>
  <v-card-text>Please contact admin to update your credit</v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="credit_confirm = false" >
     close
    </v-btn>
  
  </v-card-actions>
</v-card>
</v-dialog>


<v-dialog  v-model="extend_confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title class="headline">
  Confirm
  </v-card-title>
  <v-card-text>Please confirm your extend</v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="extend_confirm = false" >
     close
    </v-btn>
    <v-btn  color="green darken-1"  text @click=" addExtend(client)" >
    Agree
   </v-btn>
  
  </v-card-actions>
</v-card>
</v-dialog>



<v-card>
   <v-card>
    <v-card-title>Settings </v-card-title>
    <v-card-text>
    Your credit: {{credit}}
    </v-card-text>
    </v-card>
    <v-expansion-panels >
    <v-expansion-panel>
      <v-expansion-panel-header>
       Add device <v-icon left>mdi-cog-outline</v-icon>
      </v-expansion-panel-header>
      <v-expansion-panel-content>
      <v-row><v-col sm="4">
      <v-select label="Tariff" v-model="playlist.tariff" @change="SelectCountries(playlist.tariff)"  :error-messages="TariffErrors" required @input="$v.playlist.tariff.$touch()" @blur="$v.playlist.tariff.$touch()" item-value="id" item-text="name" :items="packages"></v-select>
      </v-col></v-row>
      <v-tabs>
      <v-tab>Playlist</v-tab>
      <v-tab>Ministra</v-tab>
      <v-tab>Iptv</v-tab>
  
      <v-tab-item>
      <v-card>
      <v-card-title>Playlist</v-card-title>
      <v-card-text>
      <v-form dense>
      <v-row>
      <v-col sm="4">
      
      <v-text-field label="Token" v-model="playlist.token" ></v-text-field>
      </v-col>
      <v-col sm="3"><v-btn small>change</v-btn></v-col>
      </v-row>
      <v-row>
      <v-col sm="3">
     
  
      <v-select v-show="type" :value="playlist_countries"  :items="categories" :loading="this.$store.state.loaded" chip 
      clearable item-text="title" item-value="id" label="Countries Package" multiple return-object>
    </v-select>

    
  
      </v-col>
      </v-row>
      <v-row>
      <v-col>
     
    
      <v-btn small :disabled="false" @click="savePlaylistDevice()">Add</v-btn>
      </v-col>
      </v-row>
      </v-form>
      </v-card-text>
     
      </v-card>
      </v-tab-item>
  
      <v-tab-item>
      <v-card>
      <v-card-title>Stalker device  </v-card-title>
      <v-card-text>
      <v-form dense>
      <v-row>
      <v-col sm="5"> <v-select label="Ministra packages" v-model="playlist.stalker_tariff" item-value="id" item-text="name" :items="stalker_tariffs"></v-select></v-col>
    <v-col sm="3"><v-btn @click="saveStalkerDevice()">Add</v-btn></v-col>
      </v-row>
      </v-form>
    </v-card-text>
   
    </v-card-actions>
      </v-card>
      </v-tab-item>
  
      <v-tab-item>
      <v-card>
      <v-card-title>IptvPortal devices </v-card-title>
      <v-card-text>
      <v-form dense>
      <v-row>
     
      <v-col sm="5"> <v-select label="Portal packages"  multiple v-model="playlist.iptvportal_tariff" item-value="id" item-text="name" :items="portal_tariffs"></v-select></v-col>
      <v-col sm="3"><v-btn  @click="saveIptvPortalDevice()">Add</v-btn></v-col>
      </v-row>
      </v-form>
    
      </v-card-text>
     </v-card>
      </v-tab-item>
     </v-tabs>

      
      </v-expansion-panel-content>
    </v-expansion-panel>
    <v-expansion-panel>
    <v-expansion-panel-header>
    Extend <v-icon left>mdi-cog-outline</v-icon>
   </v-expansion-panel-header>
   <v-expansion-panel-content>
   <v-row>
   <v-col sm="4">
    <v-select dense :items="packages" @change="confirmExtend()" v-model="extend" item-text="name"  selected item-value="id"  label="Extend">
  </v-select>
  </v-col>
  </v-row>
  </v-expansion-panel-content>
    </v-expansion-panel>

  </v-expansion-panels>
   
  
<v-data-table :headers="headers" :items="devices"  >
<template v-slot:item.type="{ item }">
<span v-if="item.type == 'playlist'">Playlist</span>
<span v-if="item.type == 'ministra'">Ministra</span>
<span v-if="item.type == 'portal'">Iptv portla</span>
</template>
<template v-slot:item.playlist="{ item }" >
<v-dialog v-model="dialogplaylist"  width="500" :hide-overlay="true"  :persistent="true"  >
      
    <v-card>
      <v-card-title
        class="headline grey lighten-2"
        primary-title
      >
     Playlist type
      </v-card-title>

      <v-card-text>
    
      <v-form >
      <div v-for="link in links" >
      <v-text-field :value="link.url+'/'+item.token+'/'+link.type +'/playlist'+ link.ext" :label="link.type" ></v-text-field>
      <v-btn link class="ma-2"  :href="link.url+'/'+item.token+'/'+link.type +'/playlist'+ link.ext" download ><v-icon >fas fa-download</v-icon>{{link.type}}</v-btn>

      </div>
    </v-form>
      </v-card-text>
    <v-divider></v-divider>
 <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn color="primary"  text @click="dialogplaylist=false" >
        close
        </v-btn>
      </v-card-actions>
    </v-card>
    <template v-slot:activator="{ on, attrs }">
    <v-btn text v-bind="attrs" v-on="on">
      Playlist
    </v-btn>
  </template>
  </v-dialog>


</template>
<template v-slot:item.extends="{ item }" >



</template>

<template v-slot:item.actions="{ item }">

<v-dialog  v-model="confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title class="headline">
   Delete device ?
  </v-card-title>
  <v-card-text></v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="confirm = false" >
      Disagree
    </v-btn>
    <v-btn text v-if="item.type == 'playlist'"  @click="deletePlaylist(item)"> Agree</v-btn>
    <v-btn text v-if="item.type == 'ministra'"> Agree</v-btn>
    <v-btn text v-if="item.type == 'portal'" @click="deletePortalUser(item)">Agree</v-btn>
 
  </v-card-actions>
</v-card>
</v-dialog>
<v-btn small text @click="confirmDelete(item)"><v-icon>mdi-delete</v-icon></v-btn>

</template>

  </v-data-table>
  <v-card-actions>
  <v-btn text color="  darken-1"  @click="close">
  <v-icon >mdi-close-circle-outline</v-icon>
  </v-btn>
  </v-card-actions>
  </v-card>
    
   
    
    </div>` ,
    data:() =>({
      duration: [{id:1 , val:"years"},{id:2 ,val:"months"}, {id:3 ,val:"days"} ,{id:4, val:"hours"}],
      extend_confirm : false,
        search: '',
        type : true,
        iptvportal_tariff_change : [],
        playlist : {
          token : "test" ,
           tariff : [] ,
           stalker_tarrif :[],
           iptvportal_tarrif :[]
         },
         confirm : false,
         credit_confirm : false,
         extend : [],
      
        bouquest : ['Standart', 'Edit List'],
        extends : ["Day","Month"],
        headers: [{text : 'id', value: "id"},
        {text : 'client id', value: "client_id"},
        {text : 'login', value: "name"},

        {text : 'type', value: "type"},
        {text:'Playlist' ,value:"playlist"},
        {text:'Extend' ,value:"extends"},
        {text:'Actions' ,value:"actions"},
       
        ],
        ministra_devices : [{
            number:1,
        }],
       
        dialog_cat: false,
        channels : [],
       
        list: [],
        del_id : null,
        del_play_id :null,
        dialogplaylist : false,
    
      

    }),
    validations:{
      playlist:{
        tariff:{required}
        

      }
    },
    created(){
     // this.playlist  = this.client
     this.$store.dispatch('getPackages')
     this.$store.dispatch('getCountries')
    
     this.$store.dispatch('getStalkerTariffs')
     this.$store.dispatch('getPortalTariffs')

    },
    mounted(){
     
    },
    computed :{
          packages : function(){
              return this.$store.state.packages
          },
          categories: function(){
              return this.$store.state.countries
          },
          devices: function(){
            return this.$store.state.devices
        },
        stalker_tariffs : function(){
          return this.$store.state.stalker_tariffs
        },
        portal_tariffs : function(){
          return this.$store.state.portal_tariffs
        },

        playlist_countries: function(){
          return this.$store.state. countries_by_package
        },

        credit : function(){
          return this.$store.state.account.balance
        },
        role : function(){
          return this.$store.state.account.user.role
        },
        links: function(){
          return this.$store.state.linkUrls
        },
        TariffErrors() {
          const errors = []
          if (!this.$v.playlist.tariff.$dirty) return errors
          !this.$v.playlist.tariff.required && errors.push('tariff required')
      
          return errors
        },
  


          
    },
    mounted(){
      this.$store.commit('load_links')
    },
    methods:{

      SelectCountries(tariff){
       this.$store.dispatch("CountriesByPackage",{id:tariff})
     },

      savePlaylistDevice(){
        if(!this.$v.$invalid){
          this.$store.dispatch("addDevicePlaylist",{client:this.client, device:this.playlist , countries : this.playlist_countries})
        }
      
      },

      deletePlaylist(item){
        this.$store.dispatch("deletePlaylist",{id:this.del_play_id})
        this.confirm = false
      },

      saveStalkerDevice(){
        this.$store.dispatch("addDeviceStalker",{client:this.client, device:this.playlist})

      },

      saveIptvPortalDevice(){
        
          if(!this.$v.$invalid){
            this.$store.dispatch("addDeviceIptvPortal",{client:this.client, device:this.playlist})
          
          
        }
        else{
          if(this.credit <= 0){
            this.credit_confirm = true
          }
          else{
            if(!this.$v.$invalid){
              this.$store.dispatch("addDeviceIptvPortal",{client:this.client, device:this.playlist})
            }
           
          }

        }
      
        },
        changeTariff(item){
        
          this.$store.dispatch('changeTarrif',{id:item.name, data:this.iptvportal_tariff_change})
        },

      close(){
        this.$emit('clicked',{status:false})

      },
      confirmDelete(item){
        this.confirm = true
        this.del_id  =item.name
        this.del_play_id  =item.id
     },

     deletePortalUser(item){
      this.$store.dispatch("deleteIptvPortalUser",{id:this.del_id})
      this.confirm = false
     },

     confirmExtend(item){
       this.extend_confirm  =true
       
     },

     addExtend(item){
      if(this.role == "admin"){
        this.$store.dispatch('updateExtend',{id:this.client.id,package:this.extend})
        this.extend_confirm  = false
      }
      else{

      
      if(this.credit <= 0){
        this.credit_confirm = true
      }
      else{
        console.log(this.extend)
        this.$store.dispatch('updateExtend',{id:this.client.id,package:this.extend})
        this.extend_confirm  = false
  
      }
    }
      
     }
      

    }
}