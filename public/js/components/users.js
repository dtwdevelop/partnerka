const UserSettings = ()=>import('./setting-users.js')
const Client = ()=>import('./client.js')

export default {
  name: "User",
  components :{UserSettings ,Client},
  template: `
  
    <div >
   

    <v-dialog v-model="dialogsetting" width="800" >
    <UserSettings :client="client" @clicked="close"></UserSettings>
  
    </v-dialog>

    <v-dialog v-model="dialog" width="600">
   <v-card>
    <v-card-title>Client</v-card-title>
    <v-card-text>
    <Client @clicked="closeUser"></Client>
    </v-card-text>
    <v-card-actions>
    <v-btn color="blue darken-1" text @click="dialog = false">
    Close
  </v-btn>
    </v-card-actions>
    </v-card>
    </v-dialog>


    <v-card>
    <v-card-title>
      <v-text-field  v-model="search" append-icon="mdi-magnify" label="Search" single-line hide-details></v-text-field>
    </v-card-title>
    <v-btn small @click="dialog =!dialog" color=" lighten-2" dark >add <v-icon small>mdi-plus</v-icon></v-btn>
    <v-data-table :dense="true" :headers="headers" :items="clients" :search="search" >
    

  <template v-slot:item.settings="{ item }">
  <v-btn text @click="showSetting(item)"> <v-icon>mdi-cogs</v-icon></v-btn>
</template>

<template v-slot:item.status="{ item }">
<v-switch v-model="item.status" @change="changeClientStatus(item)" ></v-switch>
</template>

<template v-slot:item.note="props">
    <v-edit-dialog :return-value.sync="props.item.note"  large  @save="onChangeNote(props.item)" > 
    <v-icon>mdi-square-edit-outline </v-icon>
     <template v-slot:input>
    <v-textarea outlined  v-model="props.item.note"   label="description"></v-textarea>
    </template>
 </v-edit-dialog>
 </template>
 <template v-slot:item.ticket="{ item }">
 <v-btn small  text ><v-icon>mdi-ticket</v-icon></v-btn>

 </template>


<template v-slot:item.action="{ item }">
<v-dialog  v-model="confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title v-if="account.user.role =='admin'" class="headline">
   Delete user ?
  </v-card-title>
  <v-card-title v-else class="headline">
 Send request to admin ?
 </v-card-title>
  <v-card-text>
  
  </v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="confirm = false" >
      Disagree
    </v-btn>
    <v-btn   v-if="account.user.role =='admin'" color="green darken-1" text @click="deleteClient(item)" >
      Agree
    </v-btn>
    <v-btn  v-else color="green darken-1" text @click="deleteClient(item)" >
 Delete
  </v-btn>
  </v-card-actions>
</v-card>
</v-dialog>
<v-btn small  text @click="confirmDelete(item)"><v-icon>mdi-delete</v-icon></v-btn>


</template>
    </v-data-table>
   
  </v-card>
  <v-snackbar v-model="snackbar">
  {{ text }}

  <template v-slot:action="{ attrs }">
    <v-btn color="pink" text v-bind="attrs"  @click="snackbar = false">
      Close
    </v-btn>
  </template>
</v-snackbar>
   </div>
     `,
  data: () => ({
    dialog: false,
    search : '',
    dialogsetting : false,
    client: null,
    confirm: false,
    snackbar : false,
    text : 'User created',
    del_id :null,
    headers: [
      {
        text: 'id',
        value :'id'
      
      },
      { text: 'name', value: 'name' },
      { text: 'email', value: 'email' },
    
      { text: 'valid to', value: 'expire' },
      { text: 'status', value: 'status' },
      { text: 'ip', value: 'ip' },
      { text: 'settings', value: 'settings' },
      { text: 'note', value: 'note' },
      { text: 'ticket', value: 'ticket' },
      { text: 'actions', value: 'action' },
      { text: 'playlist', value: 'playlist' },

     
    ],
  }),

  computed:{
    clients: function(){
     return this.$store.state.clients
    },
    account : function() {
      return   this.$store.state.account
    },

  },
    
  cretaed(){
   
  },
    
    mounted() {
      this.$store.dispatch('getClients')
   },

  
 

  methods: {
    confirmDelete(item){
       this.confirm = true
       this.del_id  = item.id
    },

    deleteClient(item){
       console.log(this.del_id )
        this.$store.dispatch('destroyClient',{id:this.del_id })
        this.confirm = false
      
     
    },

    showSetting(user) {
      this.$store.dispatch('getDevices',{id:user.id})
       this.client = user
       this.dialogsetting = true
    },

    close(event){
     this.dialogsetting = event.status
    },

    onChangeNote(item){
      this.$store.dispatch("changeClientNote",{id:item.id, desc:item.note})
     
    },

    changeClientStatus(item ) {
      this.$store.dispatch("changeClientStatus", {id:item.id , status:item.status})
    },

    closeUser(event){
         this.dialog = false
         this.snackbar = true
    }

   
}

}