export default {
    name : 'Dashboard',
    template: `<div>
<v-container>
<v-row>
<v-col sm="5" >
<v-card class="ma-2">
<v-card-title>Admin</v-card-title>
<v-card-text> <v-icon large>mdi-account-star</v-icon></v-card-text>
<v-card-actions>
</v-card-actions><v-btn  small>more..</v-btn>
</v-card-action>
</V-card>

<v-card >
<v-card-title>Dealers</v-card-title>
<v-card-text> <v-icon large>mdi-account-settings</v-icon></v-card-text>
<v-card-actions>
</v-card-actions><v-btn  small>more..</v-btn>
</v-card-action>
</V-card>

</v-col>

<v-col sm="5">
<v-card class="ma-2">
<v-card-title>Notify</v-card-title>
<v-card-text> <v-icon large>mdi-post-outline</v-icon></v-card-text>
<v-card-actions>
</v-card-actions><v-btn  to="/notifies" small>more..</v-btn>
</v-card-action>
</V-card>

<v-card>
<v-card-title>Settings</v-card-title>
<v-card-text> <v-icon large>mdi-cogs</v-icon></v-card-text>
<v-card-actions>
</v-card-actions><v-btn  small to="/settings">more..</v-btn>
</v-card-action>
</V-card>

</v-col>
</v-row>
</v-container>
    </div>`,
    data:()=>({
        lists : []
    }),
    computed :{

    },
    methods :{
        
    }

}