var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer,decimal } = window.validators
export default{
    name : 'Generator',
    mixins: [validationMixin],
    template: `
    <div>
  <v-form dense ref="form" v-if="count_lisence">
   
   <v-row>
   <v-col sm="5">
   <v-textarea v-model="client.key" label="Request Key" :error-messages="descriptionErrors" required @input="$v.client.key.$touch()" @blur="$v.client.key.$touch()"></v-textarea>
  </v-col>
  <v-col sm="5">
  <v-textarea v-show="status" v-model="client.keyresponse" label="Response Key" ></v-textarea>
 </v-col>
  
   </v-row>
 <v-row>
   <v-col><v-btn small @click=" generateKey()">generateKey</v-btn></v-col>
    </v-row>
    </v-form>
    <v-alert border="right"color="blue-grey" dark v-else>Contact admin more lisence count</v-alert>
  </div>`,
    data: () => ({
     
       
      status : false,
      
        client:{
          key : "",
          keyresponse : "",
           }


    }),
    validations:{
      client:{
        key:{required},
      

      }
    },
    computed:{
        descriptionErrors() {
        const errors = []
        if (!this.$v.client.key.$dirty) return errors
        !this.$v.client.key.required && errors.push('Request Key required')
       
        return errors
      },

      account: function(){
        return this.$store.state.account
      
      },

      count_lisence: function (){
         if(this.$store.state.account.user.count > 0){
           return true
         }
         else{
           return  false
         }
      }
    
   

     

    },
    mounted(){
      console.log()

    },
    methods:{
      generateKey(){
        const $this = this
        $this.status =  false
        this.$v.$touch();
        if(!this.$v.$invalid){
           this.$store.dispatch('GetLicence',{'key':this.client.key}).then((data)=>{
              $this.status =  true
              $this.client.keyresponse  = data
              $this.$gstore.dispatch('updateCountLisence',{id:this.account.user.id})
           })
             
        }
      
      }

    }
}