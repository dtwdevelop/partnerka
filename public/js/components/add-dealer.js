var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer,decimal } = window.validators
export default{
    name : 'addDealer',
    mixins: [validationMixin],
    template: `
    <div>
  <v-form dense ref="form">
    <v-row>
   <v-col sm="4"> <v-text-field v-model="client.name" label="Name" :error-messages="nameErrors" required @input="$v.client.name.$touch()" @blur="$v.client.name.$touch()" ></v-text-field></v-col>
   <v-col sm="4"> <v-text-field v-model="client.email" label="Email"  :error-messages="emailErrors" required @input="$v.client.email.$touch()" @blur="$v.client.email.$touch()" ></v-text-field></v-col>
 </v-row>
   <v-row>
   <v-col sm="5">
   <v-text-field v-model="client.password" :append-icon="show1 ? 'mdi-eye' : 'mdi-eye-off'" :type="show1 ? 'text' : 'password'"
   name="input-10-1" label="password" hint="At least 8 characters" @click:append="show1 = !show1" :error-messages="passwordErrors" required @input="$v.client.password.$touch()" @blur="$v.client.password.$touch()"></v-text-field>
   <v-select label="Roles" v-model="client.role" item-value="id" item-text="text" :items="roles"></v-select>
   <v-text-field v-model="client.count" label="count for licence"  :error-messages="countErrors" required @input="$v.client.count.$touch()" @blur="$v.client.count.$touch()" ></v-text-field>
   <v-textarea v-model="client.note" label="Note" :error-messages="descriptionErrors" required @input="$v.client.note.$touch()" @blur="$v.client.note.$touch()"></v-textarea>
  </v-col>
  <v-col sm="4"> <v-text-field v-model="client.credit" label="credit"  :error-messages="creditErrors" required @input="$v.client.credit.$touch()" @blur="$v.client.credit.$touch()" ></v-text-field></v-col>
 

  </v-row>
 <v-row>
   <v-col><v-btn small @click="createClient">save</v-btn></v-col>
    </v-row>
    </v-form>
  </div>`,
    data: () => ({
      show1: false,
        menu: false,
        date: new Date().toISOString().substr(0, 10),
        roles :[
          {id: "admin" , text: 'Admin'},
          {id: "dealer" , text: 'Dealer'},
          {id: "seller" , text: 'Seller'},
    
        ],
        client:{
            name : '',
            email : '',
            password : '',
            note : '',
            role : [],
            credit : 0.00,
            count : 0,
           }

    }),
    validations:{
      client:{
        name:{required,minLength:minLength(4)},
        email:{required ,email,minLength:minLength(4)},
        note:{required ,minLength:minLength(4)},
        password:{required ,minLength:minLength(4)},
        credit:{required,decimal},
        count:{required,integer},

      }
    },
    computed:{
      nameErrors() {
        const errors = []
        if (!this.$v.client.name.$dirty) return errors
        !this.$v.client.name.required && errors.push('Name required')
        !this.$v.client.name.minLength && errors.push('Description must be more than 4 symbols')
        return errors
      },
      creditErrors() {
        const errors = []
        if (!this.$v.client.credit.$dirty) return errors
        !this.$v.client.credit.required && errors.push('credit required')
        !this.$v.client.credit.decimal && errors.push('credit must be decimal')
        return errors
      },
      countErrors() {
        const errors = []
        if (!this.$v.client.count.$dirty) return errors
        !this.$v.client.count.required && errors.push('count required')
        !this.$v.client.count.integer && errors.push('count must be integer')
        return errors
      },

      descriptionErrors() {
        const errors = []
        if (!this.$v.client.note.$dirty) return errors
        !this.$v.client.note.required && errors.push('Description required')
        !this.$v.client.note.minLength && errors.push('Description must be more than 4 symbols')
        return errors
      },
      emailErrors() {
        const errors = []
        if (!this.$v.client.email.$dirty) return errors
        !this.$v.client.email.required && errors.push('Description required')
        !this.$v.client.email.minLength && errors.push('Description must be more than 4 symbols')
        !this.$v.client.email.email && errors.push('email format werong')
        return errors
      },

      passwordErrors() {
        const errors = []
        if (!this.$v.client.password.$dirty) return errors
        !this.$v.client.password.required && errors.push('Description required')
        !this.$v.client.password.minLength && errors.push('Description must be more than 4 symbols')
        
        return errors
      },

    },
    mounted(){
      

    },
    methods:{
      createClient(){
        const $this = this
        this.$v.$touch();
        if(!this.$v.$invalid){
        
              this.$store.dispatch('addUser',{client:this.client}).then(()=>{
                $this.$refs.form.reset()
                $this.$refs.form.resetValidation()
                $this.$emit('clicked')
              })
             
        }
      
      }

    }
}