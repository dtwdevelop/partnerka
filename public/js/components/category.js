const  Details = ()=> import( './details.js')
var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email, integer } = window.validators
export default {
  name : 'Categories',
  components: { Details },
  mixins: [validationMixin],
  template: `
    <div >
   <v-dialog v-model="dialog3" width="700">
     <Details @clicked="close()" /> 
    
    </v-dialog>
    <v-card color="primary" dark  v-if="loading" >
        <v-card-text>
          Загрузка ждите
          <v-progress-linear
            indeterminate
            color="white"
            class="mb-0"
          ></v-progress-linear>
        </v-card-text>
      </v-card>

      <v-dialog  v-model="dialog4"  width="500" persistent>
     

      <v-card>
        <v-card-title class="headline grey lighten-2" primary-title >
       add
        </v-card-title>

        <v-card-text>
        <v-autocomplete v-model="package.select3" :items="items" :loading="this.$store.state.loaded" :search-input.sync="search2" chips
      clearable
      hide-details
      hide-selected
      item-text="title"
      item-value="chanel"
     label="channel name"
    multiple return-object
    >
    
      <template v-slot:selection="{ attr, on, item, selected }">
        <v-chip v-bind="attr" :input-value="selected" color="blue-grey" class="white--text" v-on="on" >
        <v-icon>fa-file </v-icon>
          {{ item.title }}
        
        </v-chip>
      </template>

      <template v-slot:item="{ item }">

       <v-list-item-content>
          <v-list-item-title v-text="item.title"></v-list-item-title>
          <v-text-field v-model="item.dataid"  label="epg id" required ></v-text-field>
        </v-list-item-content>
        <v-list-item-action>
          <v-icon>>fas fa-file</v-icon>
        </v-list-item-action>
      </template>

</v-autocomplete>

        </v-card-text>

        <v-divider></v-divider>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="green darken-1" text  @click="AddChanels(add_id)"> add</v-btn>
          <v-btn color="primary"  text  @click="dialog4 = false"  >
          close
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    
    
    

    <v-dialog v-model="dialog" width="700"  hide-overlay transition="dialog-bottom-transition">
    <template v-slot:activator="{ on }">
      <v-btn small color=" lighten-2" dark v-on="on">
       add channel
      </v-btn>
    </template>
    <v-card>
      <v-card-title class="headline grey lighten-2" primary-title >
    Categories
      </v-card-title>
      <v-card-text>
      <v-form v-model="valid">
      <v-container>
        <v-row> <v-col cols="12"  md="4">
            <v-text-field v-model="package.title" label="name"  required  :error-messages="nameErrors" required @input="$v.package.title.$touch()" @blur="$v.package.title.$touch()"  ></v-text-field>
          </v-col>
          </v-row>
          <v-row>
          <v-col  cols="12"  md="6"  > 
       <v-textarea v-model="package.description" label="description" :value="package.description" :error-messages="descriptionErrors" required @input="$v.package.description.$touch()" @blur="$v.package.description.$touch()" ></v-textarea>
    


 <v-autocomplete v-model="package.select" :items="items" :loading="this.$store.state.loaded" :search-input.sync="search2" chips
      clearable
      hide-details
      hide-selected
      item-text="title"
      item-value="chanel"
     label="channel name"
    multiple return-object
    >
    
      <template v-slot:selection="{ attr, on, item, selected }">
        <v-chip v-bind="attr" :input-value="selected" color="blue-grey" class="white--text" v-on="on" >
        <v-icon>fa-file </v-icon>
          {{ item.title }}
        
        </v-chip>
      </template>

      <template v-slot:item="{ item }">

       <v-list-item-content>
          <v-list-item-title v-text="item.title"></v-list-item-title>
          <v-text-field v-model="item.dataid"  label="id epg" required ></v-text-field>
        </v-list-item-content>
        <v-list-item-action>
          <v-icon>>fas fa-file</v-icon>
        </v-list-item-action>
      </template>

</v-autocomplete>

         </v-col>
         <v-col>
         <v-autocomplete v-model="package.select4" :items="countries" :loading="this.$store.state.loaded" :search-input.sync="search3" chips
      clearable
      hide-details
      hide-selected
      item-text="title"
      item-value="id"
     label="Countries"
    multiple return-object
    >
    </v-autocomplete>
         </v-col>
        </v-row>
        <v-row>
        <v-col cols="12"  md="4">
        <v-btn color="primary" class="mr-4" @click="addChanel">add</v-btn>
      </v-col>
      </v-row>
      </v-container>
  
    </v-form>
      </v-card-text>
       <v-divider></v-divider>
       <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn
          color="primary"
          text
          @click="dialog = false"
        >
      close
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
  
     <v-card>
    <v-card-title>
    Categories
      <v-spacer></v-spacer>
      <v-text-field v-model="search" label="search" single-line hide-details ></v-text-field>
    </v-card-title>

    <v-data-table v-if="loader" :dense="true" :headers="headers" :items="packages" :search="search"  loading-text="Loading... Please wait">
    <template v-slot:item.chanels="{ item }">
    <v-icon @click="showAddChanel(item.id)">mdi-plus</v-icon>
  </template>
  <template v-slot:item.action="{ item }">
  <v-btn link small text   @click="loadPackage(item.id)">
  <v-icon small >mdi-pencil </v-icon>
  </v-btn>
  <v-dialog  v-model="confirm" persistent max-width="300" :retain-focus="false">
    
<v-card>
  <v-card-title class="headline">
   Delete category ?
  </v-card-title>
  <v-card-text></v-card-text>
  <v-card-actions>
    <v-spacer></v-spacer>
    <v-btn  color="green darken-1"  text @click="confirm = false" >
      Disagree
    </v-btn>
    <v-btn  color="green darken-1" text @click="deletePackage(item.id)" >
      Agree
    </v-btn>
  </v-card-actions>
</v-card>
</v-dialog>
  <v-btn small text  @click="confirmDelete(item)">
  <v-icon small >mdi-delete </v-icon>
  </v-btn>
 
  </template>

    </v-data-table>
    <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
    <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
  </v-sheet>
  </v-card>
  
  <v-dialog v-model="dialog2" persistent max-width="290">
      
      <v-card>
        <v-card-title class="headline">delete</v-card-title>
        <v-card-text>Удалить категорию</v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="green darken-1" text @click="dialog2 = false">Disagree</v-btn>
          <v-btn color="green darken-1" text @click="dialog2 = false">Agree</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
     </div>
     `,
  data: () => ({
   
    add_id: null,
    dialog3: false,
    search: '',
    search2: '',
    search3:  '',
    dialog: false,
    dialog2: false,
    loading: false,
    dialog4: false,
    loader : false ,
    confirm : false,
    package: {
      title: '',
      price: '',
      description: '',
      select: [],
      urls: [],
      dataid: [],
      select2: [],
      select3: [],
      select4 :[],
    },
    valid: false,
    but: {
      show: false,
      id: 0,
    },
    del_id: null,

   

  }
  ),
  validations: {
   package:{
     title:{
       required,
     },
     description :{
       required,
       minLength :minLength(4)
     }
   }
   
 
   },

  computed: {
    packages: function () {
      return this.$store.state.categories
    },
    headers: function(){
      return  [
        {
          text: 'id',
          align: 'left',
          sortable: true,
          value: 'id',
        },
        {
          text: 'title',
          align: 'left',
          sortable: false,
          value: 'title',
        },
        { text: 'description', value: 'description' },
        { text:  'chanels', value: 'chanels' },
       
        { text: 'actions', value: 'action' },
      ]

    },
    items: function () {
      return this.$store.state.channels
    },
    status: function () {
      return this.$store.state.status
    },
    linkUrl: function () {
      return this.$store.state.linkUrl
    },
    countries: function(){
      return this.$store.state.countries
    },
    nameErrors() {
      const errors = []
      if (!this.$v.package.title.$dirty) return errors
      !this.$v.package.title.required && errors.push('Name required')
      return errors
    },

    descriptionErrors() {
      const errors = []
      if (!this.$v.package.description.$dirty) return errors
      !this.$v.package.description.required && errors.push('Description required')
      !this.$v.package.description.minLength && errors.push('Description must be more than 4 symbols')
      return errors
    },

  },

  mounted() {
    const $this = this
   
    //this.$store.dispatch('ChanelsFromJson')
    this.$store.dispatch('getCategory').then(()=>$this.loader=true);
    this.$store.dispatch('getChannels');
    this.$store.dispatch('getCountries');
  },

  methods: {
    addChanel() {
      const $this  = this
      this.$v.$touch();
      if(!this.$v.$invalid){
        this.$store.dispatch("AddCategory", { package: this.package }).then(()=> console.log("add new category"))
        this.dialog = false
          
      }
     
     

    },
    deletePackage(item) {
      // this.dialog2 = true,
   
      this.$store.dispatch('deleteCategory', { id: this.del_id })
      this.confirm  = false
    },

    close(val) {
      this.dialog3 = false
    },

    async loadPackage(id) {
      this.loading = true
      const $this = this
      this.$store.dispatch("editCategory", { id: id }).then(() => {
        $this.loading = false
        this.dialog3 = true
      })

    },
    AddChanels(id) {
      this.$store.dispatch("addChanel", { id: id, select: this.package.select3 })
      this.package.select3 = []
     
    },

    showAddChanel(id) {
      this.dialog4 = true
      this.add_id = id

    },
   
      confirmDelete(item){
        this.confirm = true
        this.del_id = item.id
     },
   

  }
}
