const LineChart = Vue.component('line-chart', {
  extends: VueChartJs.Bar,
  mounted () {
    const month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль' ,'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ]
    this.renderChart({
      labels: month,
      datasets: [
        {
          label: 'Количество',
          backgroundColor: '#f87979',
          borderWidth : 1,
          data: [20, 15, 10, 40, 39, 0, 0, 0, 0, 0, 0 ,0]
        }
      ]
    }, {responsive: true, maintainAspectRatio: false})
  }
})
export { LineChart }

const PieChart = Vue.component('pie-chart', {
  extends: VueChartJs.Pie,

  computed:{
    
  },

  created() {

  },

 async mounted () {
  const datas  = [];
  const labels  = [];
  const $this  = this
    this.$store.dispatch("getPopularChanels",{limit:10}).then(()=>{
      this.$store.state.popular_chanels.forEach(chanel => {
      
        datas.push(chanel.total)
        labels.push(chanel.chanel)
      });
   
      this.renderChart({
        labels: labels,
        datasets: [
          {
            label: 'Количество',
            backgroundColor: ['#9e0000' ,'#9e004f' ,'#00289e' ,'#009e28' , '#ff1a75' ,'#4f9e00' ,'#9e4f00' ,'#ffff1a' , '#ff3300' , '	#000000'],
            data: datas
          }
        ]
      }, {responsive: true, maintainAspectRatio: false})

    })
    }
})
export { PieChart }

const PieChartArchive = Vue.component('pie-chart', {
  extends: VueChartJs.Doughnut,

  computed:{
    
  },

  created() {

  },

 async mounted () {
  const datas  = [];
  const labels  = [];
  const $this  = this
    this.$store.dispatch("getPopularArchive",{limit:10, archive:true}).then(()=>{
      this.$store.state.popular_chanels_archive.forEach(chanel => {
      
        datas.push(chanel.total)
        labels.push(chanel.chanel)
      });
   
      this.renderChart({
        labels: labels,
        datasets: [
          {
            label: 'Количество',
            backgroundColor: ['#9e0000' ,'#9e004f' ,'#00289e' ,'#009e28' , '#ff1a75' ,'#4f9e00' ,'#9e4f00' ,'#ffff1a' , '#ff3300' , '	#000000'],
            data: datas
          }
        ]
      }, {responsive: true, maintainAspectRatio: false})

    })
    }
})
export { PieChartArchive }


