export default {
  name : 'Details',
  template: `
    <div>
    <v-card>
    
    <v-form >
    <v-container>
    <v-dialog v-model="dialog" width="500">
     <v-card>
        <v-card-title class="headline grey lighten-2" primary-title >
       </v-card-title>
         <v-card-text>
        <v-form v-model="chanel_f">
        <v-text-field v-model="chanel.title"  label="name" single-line   ></v-text-field>
        <v-text-field v-model="chanel.dataid"  label="id epg" single-line    ></v-text-field>
        </v-form>
        <v-btn small color="primary" @click="updateChanel(chanel.id)">save</v-btn>
        </v-card-text>
<v-divider></v-divider>
<v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="primary" text  @click="dialog = false" >
           x
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    <v-card-title>change</v-card-title>
    <v-list-item-content>
    <v-row>
   
     <v-col cols="12" sm="10">
     <v-form>
     <v-text-field  label="name" single-line v-model="categories.category.title"></v-text-field>
     <v-textarea label="description" single-line v-model="categories.category.description"> </v-textarea>
     </v-form>
     <v-btn small color="primary" @click="updateCategory(categories.category)">save</v-btn>

     <v-text-field v-model="search" label="search" single-line hide-details ></v-text-field>
     <v-data-table :headers="headers"  :search="search"  :items="items"  loading-text="Loading... Please wait">
     <template v-slot:item.logoimg="{ item }">
    
     <v-avatar v-if="item.logo != 'logo'" size="40">
    
          <v-img :src="'https://duga.tv/wp-content/uploads/icons/'+item.chanel+'.png'" alt="logo"></v-img>
     </v-avatar>
     </template>

     <template v-slot:item.icon="{ item }">
     <div>
     
     <v-file-input dense small-chips v-model="logo" accept="image/png, image/jpeg, image/jpeg, image/bmp" label="upload logo" @change="uploadLogo(item.id)"></v-file-input>
      </div>
      </template>

      <template v-slot:item.action="{ item }">
   
       <v-icon @click="setData(item)">mdi-pencil </v-icon>
       <v-icon @click="deleteChanel(item)">mdi-delete </v-icon>
      </template>
      </v-data-table>
     </v-col>
     </v-row>
      </v-list-item-content>
      <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="primary" text  @click="clickClose()" >
           x
          </v-btn>
        
        </v-card-actions>
      </v-container>
      </v-form>
      </v-card>
      </div>
      
  `,
  data: () => ({
    logo: null,
    search: '',
    chanels: [],
    category: [],
    dialog: false,
    chanel: { id: "", title: "t", dataid: "t" },
    dialog: false,
    chanel_f: false,


   

  }),
  computed: {
    categories: function () {
      return this.$store.state.category
    },
    items: function () {
      return this.$store.state.category.chanels
    },
    headers: function(){
      return [
      { text: 'id', value: 'id' },
      {
        text: 'name',
        align: 'left',
        sortable: false,
        value: 'title',
      },


      { text:  'package_id', value: 'dataid' },
      { text:  'logo', value: 'logoimg' },
      { text:  'logo_upload', value: 'icon' },
      { text: 'actions', value: 'action' },

    ]}
  },
  methods: {
    setData(item) {
      this.chanel.title = item.title
      this.chanel.dataid = item.dataid
      this.chanel.id = item.id
      this.dialog = true
    },

    deleteChanel(item) {
      this.$store.dispatch("deleteChanel", { id: item.id })
      this.$store.dispatch("loadEditCategory", { id: this.$store.state.categories.category.id })
    },

    clickClose(event) {
      this.$emit('clicked', false)
    },
    updateCategory(category) {

      this.$store.dispatch("updateCategory", { category: { id: category.id, title: category.title, description: category.description } })
    },

    updateChanel(id) {
      this.$store.dispatch("UpdateChanel", { id: id, title: this.chanel.title, dataid: this.chanel.dataid })
      this.$store.dispatch("loadEditCategory", { id: this.$store.state.categories.category.id })
      this.dialog = false

    },
    uploadLogo(ch_id){
      console.log(this.logo);
      this.$store.dispatch("uploadLogo",{logo : this.logo, id:ch_id})
      this.logo  = null
     },

  },

  mounted() {

  },

}
