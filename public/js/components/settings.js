var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email } = window.validators

export default {
  name: 'Settings',
  mixins: [validationMixin],
  template: `
    <div>
    <v-container v-if="loader">
      <v-form >
   <v-row>
        <v-col cols="2" md="6">

    <v-card class="mb-12" >
    
        <v-card-title>Settings</v-card-title>
       
        <v-card-text>
        <v-expansion-panels v-model="panel" multiple >
         <v-expansion-panel>
         <v-expansion-panel-header>Flussonic and links </v-expansion-panel-header>
         <v-expansion-panel-content>
         <v-text-field v-model="options.login"  label="flussonic login" :error-messages="loginErrors" required @input="$v.options.login.$touch()"@blur="$v.options.login.$touch()" ></v-text-field>
         <v-text-field v-model="options.password" type="password" :error-messages="passwordErrors" @input="$v.options.password.$touch()"@blur="$v.options.password.$touch()"   label="password" required ></v-text-field>
         <v-text-field v-model="options.url"   label="link api" :error-messages="urlErrors" required @input="$v.options.url.$touch()"@blur="$v.options.url.$touch()" ></v-text-field>
         <v-text-field v-model="options.domain"   label="domain url" :error-messages="domainErrors" required @input="$v.options.domain.$touch()"@blur="$v.options.domain.$touch()"  ></v-text-field>
         <v-text-field v-model="options.sub_url"   label="sub domain" :error-messages="sub_urlErrors" required @input="$v.options.sub_url.$touch()"@blur="$v.options.sub_url.$touch()"  ></v-text-field>
         <v-text-field v-model="options.epg"   label="epd link"  ></v-text-field>
         <v-text-field v-model="options.stat_link"   label="Statistic link"  ></v-text-field>
         </v-expansion-panel-content>
       </v-expansion-panel>

         <v-expansion-panel>
         <v-expansion-panel-header>Stalker</v-expansion-panel-header>
         <v-expansion-panel-content>
         <v-text-field v-model="options.login_stalker"  label="stalker login"  ></v-text-field>
         <v-text-field v-model="options.password_stalker"  label="stalker password" type="password"  required ></v-text-field>
         <v-text-field v-model="options.url_stalker"   label="stalker api link" :error-messages="url_stalkerErrors" required @input="$v.options.url_stalker.$touch()"@blur="$v.options.url_stalker.$touch()"  ></v-text-field>
         
         </v-expansion-panel-content>
       </v-expansion-panel>

       <v-expansion-panel>
       <v-expansion-panel-header>Iptv Portal </v-expansion-panel-header>
       <v-expansion-panel-content>
       <v-text-field v-model="options.portal_login"  label="login"  ></v-text-field>
       <v-text-field v-model="options.portal_password"  label="password" type="password"   ></v-text-field>
       <v-text-field v-model="options.portal_link_api"   label="api link"   ></v-text-field>
       <v-checkbox v-model="options.btn_portal_status" label="Playlist and stalker on "></v-checkbox>
       
       </v-expansion-panel-content>
     </v-expansion-panel>

</v-expansion-panels>
    </v-card-text>
    </v-card>
   
        </v-col>
       

<v-col>
<v-card>
<v-card-text>
  <v-divider></v-divider>
      <v-text-field  v-model="options.sub_limit"  label="Sub Accounts limit"  ></v-text-field>
    </v-card-text>
   <v-card-actions>
       <v-btn color="primary" small @click="SaveSettings()" class="mr-4"><v-icon small>mdi-content-save-settings</v-icon></v-btn>
       <v-btn color="primary" small  :loading="loading" :disabled="loading"  @click="LoadChannels()" class="mr-4">Load channels<v-icon small>mdi-plus</v-icon></v-btn>
     </v-card-actions>
      </v-card>
     </v-col>
        </v-row>
        </v-form>
       </v-container>
         <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
        <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
      </v-sheet>
   </div>`,
  data: () => ({
    status : "",
    show1 : true ,
    loader : true,
    panel: [],
    loading :false,

  }),

  validations: {
    options: {
      login: {
        required,
        minLength: minLength(4)
      },
      password: {
        required,
        minLength: minLength(4)
      },
      url: {
        required,
        minLength: minLength(10)
      },
      domain: {
        required,
        minLength: minLength(4)
      },
      sub_url: {
        required,
        minLength: minLength(4)
      },
      url_stalker: {
        required,
        minLength: minLength(4)
      },

    },

  },

  computed: {

    options: function () {
     
      return this.$store.state.options
    },

  

    loginErrors() {
      const errors = []
      if (!this.$v.options.login.$dirty) return errors
      !this.$v.options.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.options.login.required && errors.push('Логин обязательно.')
      return errors
    },
    urlErrors() {
      const errors = []
      if (!this.$v.options.url.$dirty) return errors
      !this.$v.options.url.minLength && errors.push('Длина логина должна быть больше 10 символов')
      !this.$v.options.url.required && errors.push('Линк обязательно.')
      return errors
    },
    passwordErrors() {
      const errors = []
      if (!this.$v.options.password.$dirty) return errors
      !this.$v.options.password.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.options.password.required && errors.push('Пароль обязательный обязательно.')
      return errors
    },

    domainErrors() {
      const errors = []
      if (!this.$v.options.domain.$dirty) return errors
      !this.$v.options.domain.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.domain.required && errors.push('Домен обязательно.')
      return errors
    },
    sub_urlErrors() {
      const errors = []
      if (!this.$v.options.sub_url.$dirty) return errors
      !this.$v.options.sub_url.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.sub_url.required && errors.push('Домен обязательно.')
      return errors
    },
    url_stalkerErrors() {
      const errors = []
      if (!this.$v.options.url_stalker.$dirty) return errors
      !this.$v.options.url_stalker.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.url_stalker.required && errors.push('Сталкер обязательно.')
      return errors
    },

},
  mounted() {
    const $this = this
   // this.$store.dispatch("GetSetting").then(function(){ $this.loader = true});

  },

 methods: {
    SaveSettings() {
      if (!this.$v.$invalid) {this.$store.dispatch("SaveSetting", { data: this.options }) }

    },
    LoadChannels(){
      const $this = this
      this.loading = true
      this.$store.dispatch('loadChannel').then(function(data){
      $this.loading =data.loading
      });
    }
  
  },

}


