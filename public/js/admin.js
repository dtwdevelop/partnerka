export default  {

     name : 'Admin',
     template: `
    <div>
    <v-app >
    <v-main>
    
<v-toolbar dense>
<v-toolbar-title>
<v-icon @click.stop="drawer = !drawer">mdi-animation-play</v-icon>

</v-toolbar-title>
<v-spacer></v-spacer>
<v-toolbar-items>
<v-btn  v-show="false" icon >
<v-icon>mdi-bell-ring </v-icon><v-badge small  bordered :content="total_notifies"  color="error"></v-badge> 
</v-btn>

<v-btn v-show="isAuth && role == 'admin ' ||  isAuth && role == 'superadmin '" to="/dashboard"><v-icon>mdi-view-dashboard</v-icon></v-btn>
<v-btn  v-show="isAuth" icon @click="logOut()">
<v-icon>mdi-export</v-icon>
</v-btn>
</v-toolbar-items>
</v-toolbar>

<v-container>
<v-row>

<v-navigation-drawer v-model="drawer" absolute bottom temporary >

<v-subheader  v-if="isAuth" >

        Credit : {{account.balance}}
        <v-avatar color="silver">
      <v-btn text to="/profile" large><v-icon dark> mdi-account-circle </v-icon></v-btn>
</v-avatar>
      </v-subheader>
  
   <v-list v-show="isAuth">
      <v-list-group  v-if="role == item.role" v-for="item in items":key="item.title" v-model="item.active" :prepend-icon="item.action" no-action>
        <template v-slot:activator>
          <v-list-item-content>
            <v-list-item-title v-text="item.title" ></v-list-item-title>
          </v-list-item-content> 
        </template>

        <v-list-item v-if="role == item.role" v-for="child in item.items" :key="child.title">
          <v-list-item-content  >
            <v-btn text :to="child.to" class="mr-5">{{child.title}}<v-icon>{{child.icon}}</v-icon></v-btn>
            
          </v-list-item-content>
        </v-list-item>
      </v-list-group>
    </v-list>
 
  </v-navigation-drawer>
  
  <v-col sm="12">
  <router-view></router-view>


  </v-col>
  </v-row>
  </v-container>
   </v-main>
  </v-app>
   </div>`,
   data: () => ({
    sticky: true,
    items: [
      {
        action: 'mdi-account',
        items: [
          { title: 'Dealers ', to: '/dealers' , icon:'mdi-account-tie'   },
          { title: 'Packages ', to: '/packages' , icon:'mdi-view-list'  },
          { title: 'Categories ', to: '/categories' , icon:'mdi-format-list-bulleted' },
          { title: 'Clients ', to: '/users' ,   icon:'mdi-account-multiple' },
          { title: 'Generator ', to: '/generator' , icon:'mdi-table-settings'   },
          { title: 'Statistics', to: '/statistic' , icon:'mdi-chart-bar'   },

        ],
        title: 'Admin',
         role: 'admin'
      },

      {
        action: 'mdi-account',
        items: [
          { title: 'Dealers ', to: '/dealers' , icon:'mdi-account-tie'   },
          { title: 'Packages ', to: '/packages' , icon:'mdi-view-list'  },
          { title: 'Categories ', to: '/categories' , icon:'mdi-format-list-bulleted' },
          { title: 'Clients ', to: '/users' ,   icon:'mdi-account-multiple' },
          { title: 'Generator ', to: '/generator' , icon:'mdi-table-settings'   },
          { title: 'Statistics', to: '/statistic' , icon:'mdi-chart-bar'   },
          { title: 'Status', to: '/status' , icon:'mdi-view-stream'   },
          { title: 'Dashboard', to: '/dashboard' , icon:'mdi-view-dashboard'   },

        ],
        title: 'Admin',
         role: 'superadmin'
      },


      {
        action: 'mdi-account',
        items: [
          { title: 'Generator ', to: '/generator' , icon:'mdi-table-settings'   },
        

        ],
        title: 'Key',
         role: 'dealer'
      },


      {
        action: 'mdi-account-settings',
        active: true,
        items: [
          
          { title: 'Users ', to: '/users' ,   icon:'mdi-account-multiple', role: 'dealer' },
         
         
        
         
        ],
        title: 'Reseller',
        role : 'dealer'
      },
      {
        action: 'mdi-newspaper-variant',
        items: [{ title: 'Add post' }],
        title: 'Posts',
        role : 'admin'
      },
],
drawer: false
    
  }),
  computed:{
    account: function(){
      return this.$store.state.account
    },
    isAuth: function(){
      return  this.$store.state.isAuth
    },

    role : function(){
      return this.$store.state.role
    },

    total_notifies(){
      console.log(this.$store.getters.total_notifies)
      return this.$store.getters.total_notifies

    }

    
  },
  
  created(){

    if(!this.$store.state.isAuth){
      this.$router.push('/')
     }
     
    
    
},

  methods:{
    logOut(){
      const $this  = this
      this.$store.dispatch("loginOut").then(()=>{
         $this.$router.push('login')
      })
    },

    tokenRefresh(){
      setInterval(()=>{
       this.$store.dispatch('tokenRefresh')  
      },7000)

    }
   
}

}
