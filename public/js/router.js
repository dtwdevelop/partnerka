const Admin = () => import('./admin.js')
const User = () => import('./components/users.js')
const Dashboard = ()=> import('./components/dashboard.js')
const Packages  = ()=> import('./components/packages.js');
const AddPackage  = ()=> import('./components/add-package.js')
const Settings  = () => import('./components/settings.js')
const Categories  = () => import('./components/category.js')
const Dealers  = () => import('./components/dealers.js')
const Notify = () => import('./components/notify.js')
const Generator = () => import('./components/generator.js')
const Statistic = () => import('./components/statistic-sessions.js')
const Profile = () => import('./components/user-profile.js')
const StatusChannels = () => import('./components/status-channels.js')
//Account, Payment , Package
import { Develop ,   UserLogin } from './app.js';
import statusChannels from './components/status-channels.js';

const routes = [
     { path: '/', component: UserLogin    } ,
     {path: '/users', component: User , meta: { requiresAuth: true } , name:'users'},
     {path: '/dashboard', component: Dashboard , name:'dashboard' , meta: { requiresAuth: true }  },
     { path: '/generator', component: Generator , meta: { requiresAuth: true }  },
     { path: '/packages', component: Packages , meta: { requiresAuth: true }  },
     { path: '/categories', component: Categories , meta: { requiresAuth: true }  },
     { path: '/package/add', component: AddPackage , meta: { requiresAuth: true }  },
     { path: '/package/edit/:id', component: AddPackage , meta: { requiresAuth: true }  },
     {path : '/settings' , component : Settings , meta: { requiresAuth: true } },
     {path : '/dealers' , component : Dealers , meta: { requiresAuth: true } },
     {path : '/login' ,  name: 'login', component : UserLogin},
     {path : '/notifies' , component : Notify , meta: { requiresAuth: true } },
     {path : '/statistic' , component : Statistic , meta: { requiresAuth: true } },
     {path : '/profile' , component : Profile , meta: { requiresAuth: true } },
     {path : '/status' , component : statusChannels , meta: { requiresAuth: true } }

  ]

  const router = new VueRouter({
  mode: 'history',
    routes : routes // short for `routes: routes`
  })

  new Vue({ 
    store,
    router ,
   vuetify: new Vuetify(),
   render: h => h(Admin)
}).$mount("#app")

  
 router.beforeEach((to, from,next)=>{
    if (to.matched.some(record => record.meta.requiresAuth)) {
     
     if (! store.state.isAuth) {
        next({
          name: 'login',
         
       })
      } else {
        next()
      }
    } else {
      next() 
    }

  })

 