
axios.defaults.headers.common = {'Accept':  'application/json' }
var interval = null
const store = new Vuex.Store({
    state: {
        options: {
            login: "",
            password: "",
            url: "",
        },
        channels: [],
        categories: [],
        category: [],
        packages: [],
        clients: [],
        countries: [],
        devices: [],
        stalker_tariffs: [],
        portal_tariffs: [],
        users: [],
        countries_by_package: [],
        account: {
            user: null,
            token : null,
            balance : 0,
            }
        ,
        isAuth : false,
        role : '',
        package : {},
        notifies : [],
        stattic_sessions : [],
        linkUrls : [],
        loaded : true,
        channels_status : [],
       
    },

    getters: {
        token: state => {
          return state.account.token
        },

        notifies_total: state =>{
            return state.notifies.length

        }
      },

    mutations: {
        change_channels(state, payload) {
            state.channels = payload.channels
        },
        change_categories(state, payload) {
            state.categories = payload.categories
        },

        delete_category(state, payload) {
            state.categories = state.categories.filter((item)=>{
                return item.id  != payload.id
            })
        },

        change_packages(state, payload) {
            state.packages = payload.packages
        },
        change_clients(state, payload) {
            state.clients = payload.clients
        },
        change_countries(state, payload) {
            state.countries = payload.countries
        },
        change_devices(state, payload) {
            state.devices = payload.devices
        },
        change_stalker_tariffs(state, payload) {
            state.stalker_tariffs = payload.tariffs
        },
        change_portal_tariffs(state, payload) {
            state.portal_tariffs = payload.tariffs
        },
        change_users(state, payload) {
            state.users = payload.users
        },

        delete_client(state,payload){
            state.clients  = state.clients.filter((item)=>{
                return item.id != payload.id
            })

        },
        change_token_sessinon(state, payload) {
            state.stattic_sessions = payload.statistic
        },

        change_account(state,payload){
            state.account.user = payload.user
            state.account.token = payload.token
            state.account.balance  = payload.balance
            state.role = payload.user.role
           
        },

        change_countrie_by_package(state,payload){
          state.countries_by_package  = payload.countries
        },

        change_notifies(state,payload){
            state.notifies  = payload.notify

        },
        change_auth(state,payload){
            state.isAuth = payload.auth
        },
        delete_device(state,payload){
          state.devices  = state.devices.filter((device)=>{
             
              return device.name !== payload.name
          })
        },

        delete_package(state,payload){
            console.log(state.package ,payload.id)
            state.packages  = state.packages.filter((package)=>{
               return package.id !== payload.id
            })

        },

        change_package(state,payload){
            const package ={
                id : payload.package.id,
                name :  payload.package.name,
                is_trial : payload.package.is_trial,
                is_normal : payload.package.is_normal,
                is_playlist : payload.package.is_playlist,
                is_ministra : payload.package.is_ministra,
                is_iptvportal : payload.package.is_iptvportal,
                duration : null,
                duration_time: '',
                credit : payload.package.credit,
                max_connection : payload.package.max_connection,
                categories : [],
                type : false,
            }
            state.package = package
        },

        change_channels_status(state,payload){
             state.channels_status  = payload.status
        },

        load_links(state, payload) {
            state.linkUrls = [
                { url: 'http://' + document.domain + ':'+ location.port +'/package-urls', type: 'm3u8', ext: '.m3u8' },
                { url: 'http://' + document.domain + ':'+ location.port + '/package-urls', type: 'm3u', ext: '.m3u' },
                { url: 'http://' + document.domain + ':'+ location.port + '/package-urls', type: 'ottplayer', ext: '.m3u8' },
                { url: 'http://' + document.domain + ':'+ location.port +  '/package-urls', type: 'siptv', ext: '.m3u' },
                { url: 'http://' + document.domain + ':'+ location.port + '/package-urls', type: 'ssiptv', ext: '.m3u' },
                { url: 'http://' + document.domain + ':'+ location.port + '/package-urls', type: 'ottplayerm', ext: '.m3u8' },
            ]
            state.loaded = false
        },
       

       
    },

    actions: {
        /**
         * load channels to db
         * @param {*} param0 
         * @param {*} payload 
         * @returns 
         */
        async loadChannel({ state, commit }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/list-channels',headers)

                return res.data;
            }
            catch (err) {
                console.log(err)
            }
        },

        /**
         * get channels from api
         * @param {*} param0 
         * @param {*} payload 
         */
        async getChannels({ state, commit }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/channels', headers)
                commit('change_channels', { channels: res.data })

            }
            catch (err) {
                console.log(err)
            }

        },

        async getCategory({ commit, state, dispatch }, payload) {
            const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
            await axios.get('/get-categories', headers).then(function (res) {
                commit('change_categories', { categories: res.data })

            }).catch(function (erro) {
            })
        },

        async deleteCategory({ commit, state, dispatch }, payload) {
            commit('delete_category',{id:payload.id})
            const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
            await axios.delete('/category-delete?id=' + payload.id, headers).then(function (res) {
                commit('change_categories', { categories: res.data })
            }).catch(function (erro) {
            })
        },

        /**
         * add category
         * @param {*} param0 
         * @param {*} payload 
         */
        async AddCategory({ state, commit ,dispatch}, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                await axios.post('/category/add', { category: payload.package }, headers).then(function (res) {
                    dispatch("getCategory")
                }).catch(function (err) {

                })

            }
            catch (err) {
                console.log(err)
            }
        },

        async editCategory({ commit, state }, payload) {
            try {
                const acc = {}
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/category-edit?id=' + payload.id, headers)
                state.category = res.data
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        async updateCategory({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const acc = {}
                const res = await axios.put('/category-update', payload, headers)
                console.log(res.data)
                dispatch("getCategory")
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * add chanel
         * @param {*} param0 
         * @param {*} payload 
         */
        async addChanel({ commit, state, dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/add-channel', payload, headers)
                // console.log(res.data)
                dispatch("getCategory")
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * add package
         * @param {*} param0 
         * @param {*} payload 
         */
        async addPackage({ commit, state, dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/package/create', { package: payload.package }, headers)
                console.log(res.data)
               
            }
            catch (err) {
                console.log(err)
            }
        },

        /**
         * get packages
         * @param {*} param0 
         * @param {*} payload 
         */
        async getPackages({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-packages', headers)
                commit('change_packages', { packages: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

        async getPackageEdit({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-package?id='+payload.id, headers)
                commit('change_package',{package:res.data})
               return state.package

            }
            catch (err) {
                console.log(err)
            }
        },

        async deletePackage({ commit, state }, payload) {
            try {
                commit('delete_package',{id:payload.id})
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.delete('/delete-package?id=' + payload.id, headers)
                commit('change_packages', { packages: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

        async getClients({ commit, state  }, payload) {
            try {
                
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-clients', headers);

                commit('change_clients', { clients: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

        async addClient({ commit, state, dispatch }, payload) {

            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/add-client', { client: payload.client }, headers);
                dispatch("getClients")
                console.log(res.data)
            }
            catch (err) {
                console.log(err)
            }

        },
        async destroyClient({ commit, state }, payload) {
            try {
                commit("delete_client",{id:payload.id})
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.delete('/delete-client?id=' + payload.id, headers);
                console.log(res.data)
              //  commit('change_clients', { clients: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

       

        async getCountries({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/countries', headers);

                commit('change_countries', { countries: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

        async addDevicePlaylist({ commit, state, dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/add-device', { user: payload.client, device: payload.device, countries:payload.countries }, headers)
                dispatch('getDevices')

            }
            catch (err) {
                console.log(err)
            }

        },

        async addDeviceStalker({ commit, state, dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/add-stalker', { user: payload.client, device: payload.device }, headers)
                dispatch('getDevices')

            }
            catch (err) {
                console.log(err)
            }


        },

        async getDevices({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/devices?id='+payload.id, headers);
                commit('change_devices', { devices: res.data })

            }
            catch (err) {
                console.log(err)
            }

        },

        async getStalkerTariffs({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-stalker-tariff', headers);
                commit('change_stalker_tariffs', { tariffs: res.data })

            }
            catch (err) {
                console.log(err)
            }

        },

        async getPortalTariffs({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-portal-tariffs', headers);
                commit('change_portal_tariffs', { tariffs: res.data })

            }
            catch (err) {
                console.log(err)
            }

        },
        async addDeviceIptvPortal({ commit, state, dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/add-portal', { user: payload.client, device: payload.device},headers )
                dispatch('getDevices',{id:payload.client.id})

            }
            catch (err) {
                console.log(err)
            }


        },
        async deletePlaylist({ commit, state ,dispatch }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.delete('/delete-playlist?id=' + payload.id, headers);
                console.log(res.data)
                dispatch('getDevices')

            }
            catch (err) {
                console.log(err)
            }
        },

        async deleteIptvPortalUser({ commit, state, dispatch }, payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                commit('delete_device',{name:payload.id})
                const res = await axios.delete('/delete-portal?id='+payload.id, headers);
                console.log(res.data)

            }
            catch(err){
                console.log(err)
            }
        },

        async getUsers({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-users', headers);
                commit('change_users', { users: res.data })

            }
            catch (err) {
                console.log(err)
            }

        },

        async addUser({commit,state,dispatch},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.post('/add-user',{client:payload.client},headers)
                dispatch('getUsers');
            }
            catch(err){
                console.log(err)
            }
        },
        
        async destroyUser({ commit, state }, payload) {
            try {
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.delete('/delete-user?id=' + payload.id, headers);
                
                commit('change_users', { users: res.data })

            }
            catch (err) {
                console.log(err)
            }
        },

        async updateExtend({ commit, state },payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/add-extend?id='+payload.id+'&package_id='+payload.package, headers);

            }
            catch (err) {
                console.log(err)
            }

        },

        async CountriesByPackage({ commit, state },payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/get-countries-by-package?id='+payload.id, headers);
                console.log(res.data)
               commit('change_countrie_by_package',{countries:res.data})

            }
            catch (err) {
                console.log(err)
            }

        },

        async changeTarrif({commit,state,dispatch},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const acc = {}
                const res = await axios.put('/change-portall-tarrifs', {id:payload.id, tariff:payload.data}, headers)

            }
            catch(err){
                console.log(err)
            }
        },

        async loginIn({commit,state,dispatch},payload){
            try{
                const res = await axios.post('/auth/login',{email:payload.email, password:payload.password})
                commit('change_account',{user:res.data.user  ,balance:res.data.balance ,token:res.data.access_token})
                if(res.data.access_token != null){
                    commit('change_auth', {auth: true})
                    localStorage.setItem('auth',res.data.access_token)
                    localStorage.setItem('role',res.data.user.role)
                    this.dispatch('getNotifies',{id:res.data.user.id})
                   
                    dispatch('tokenRefresh') 
                    return res.data.user 
                   
                }
               
                return res.data.user
            }
            catch (err) {
                console.log(err)
                return err.data
            }
           
        },

        async tokenRefresh({commit,state,dispatch}){
            try{
              
              if(state.isAuth){
               interval = setInterval(async ()=>{
                    const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                    const res = await axios.post('/token-refresh',{}, headers)
                   
                    if(res.data.access_token != null){
                        commit('change_account',{user:res.data.user  ,balance:res.data.balance ,token:res.data.access_token})
                        commit('change_auth', {auth: true})
                        localStorage.setItem('auth',res.data.access_token)
                        this.dispatch('getNotifies',{id:res.data.user.id})
                      
                        return res.data.user 
                    }
                   
                    return res.data.user
                   
                   },50000)

              }  
            
               
            }

            catch (err) {
                console.log(err)
                return err.data
            }
        },

        async getNotifies({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.get('/notify?id='+payload.id ,headers)
                commit('change_notifies',{notify: res.data.notifies})

            }
            catch(err){
              console.log(err)
            }

        },

        async loginOut({commit,state},payload){
            try{
                
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res = await axios.post('/auth/logout',{} ,headers)
                commit('change_account',{user:{}})
                localStorage.removeItem('auth')
                localStorage.clear()
                commit('change_auth', {auth:false})
                clearInterval(interval)
            }
            catch (err) {
                console.log(err)
            }
           
        }, 

      async  changeClientNote({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.post('/change-note',{id:payload.id ,note:payload.desc}, headers)
                console.log(res.data)

            }
            catch(err){
                console.log(err)
            }
        },

        async  changeClientNote({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.post('/change-note',{id:payload.id ,note:payload.desc}, headers)
                console.log(res.data)

            }
            catch(err){
                console.log(err)
            }
        },

        async  changeClientStatus({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.put('/change-status',{id:payload.id ,status:payload.status}, headers)
                console.log(res.data)

            }
            catch(err){
                console.log(err)
            }
        },

        async  GetLicence({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.post('/get-apikey',{key:payload.key}, headers)
               return res.data.key

            }
            catch(err){
                console.log(err)
            }
        },

        async  getSessions({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.get('/token-session?version='+payload.version, headers)
               
                commit('change_token_sessinon',{statistic:res.data})
               

            }
            catch(err){
                console.log(err)
            }
        },

        async updateCountLisence({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.get('/count-lisence?id='+payload.id, headers)
               }
            catch(err){
                console.log(err)
            }
        },
        async updatePassword({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.post('/change-password',{id:payload.user_id, password: payload.password }, headers)
               return res.msg

            }
            catch(err){
                console.log(err)
            }
        },
        /**
         * get channels status 
         * @param {*} param0 
         * @param {*} payload 
         */
        async getChannelsStatus({commit,state},payload){
            try{
                const headers = {headers:{'Authorization' :'Bearer '+ state.account.token}}
                const res  = await axios.get('/channels-status', headers)
               
                commit("change_channels_status",{status: res.data.channels})
               }
            catch(err){
                console.log(err)
            }
        },



      

 }


})
