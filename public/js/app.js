// const validationMixin = window.vuelidate
// const { required, minLength ,sameAs ,email } = window.validators
Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators

const UserLogin = Vue.component('UserLogin', {
  // mixins: [validationMixin.validationMixin],
  components: {VueRecaptcha },
  template: `
  <div>
  <v-card   >
    <v-container  >
   <v-row>
      <v-col  md="4">
      <v-card-title>
     Loin in
      <v-alert  dense outlined type="error" v-if="status">
   Login failed
    </v-alert>
    
    </v-card-title>
    
      <v-form >
      <v-card-text>
      <v-text-field  prepend-icon="mdi-account" v-model="login"  label="login" :error-messages="loginErrors" required @input="$v.login.$touch()"@blur="$v.login.$touch()" ></v-text-field>
      <v-text-field  prepend-icon="mdi-lock" v-model="password" type="password" :error-messages="passwordErrors" @input="$v.password.$touch()"@blur="$v.password.$touch()"   label="password" required ></v-text-field>
    </v-card-text>
     <v-card-actions>
     <vue-recaptcha   ref="recaptcha"  @expired="onExpired"  @verify="onVerited" sitekey="6Ld1Ac8aAAAAABZjla-lUOC3m_iKkk4uZCOGoFvv"></vue-recaptcha> 
      <v-btn color="primary" :disabled="status_re" @click="UserLogin()" class="mr-4">login</v-btn>
     </v-card-actions>
      </v-form>

      </v-col>
      <v-col>
     
      </v-col>
      </v-row>
     </v-container>
      </v-card>
     </div>`,
  data: () => ({

    login: "",
    password: "",
    checkbox: "",
    no_active : false,
    status_re : true,
    response : null,
   

  }),

  computed: {
    loginErrors() {
      const errors = []
      if (!this.$v.login.$dirty) return errors
      !this.$v.login.minLength && errors.push('email must be more than 4 symbol')
      !this.$v.login.required && errors.push('email required')
      !this.$v.login.email && errors.push('invalid email format')
      return errors
    },

    status() {
      return this.$store.state.status
    },

    passwordErrors() {
      const errors = []
      if (!this.$v.password.$dirty) return errors
      !this.$v.password.minLength && errors.push('password must be more than 4 symbol')
      !this.$v.password.required && errors.push('message password required')
      
      return errors
    }


  },

  validations: {
    login: {
      required,
      minLength: minLength(4),
      email
    },
    password: {
      required,
      minLength: minLength(4)
    }
  },
  created() {

  },

  methods: {
    onEvent() {
      // when you need a reCAPTCHA challenge
     
    },
    async UserLogin() {
      this.$v.$touch();
      if (!this.$v.$invalid) {
        const $this  = this
        this.$store.dispatch("loginIn", { email: this.login, password: this.password }).then((res) => {
          
          if(res != null ){
            if(res.role == 'admin')
            this.$router.push({ name: 'dashboard' })
            else{
              this.$router.push({ name: 'users' })
            }
          }
          else{
            $this.no_active  = true
          }
          })

      }

    },
    onVerited(response){
     if(response != null){
     this.status_re  = false
     // this.$refs.recaptcha.reset();
     }
     
    }
    ,
    onExpired(){
      this.$refs.recaptcha.reset();
    }
    
  },

})

const Develop = Vue.component('Develop', {
  // mixins: [validationMixin],
  template: `
  <div>
  <v-card class="mb-12">
    <v-container>
 <v-row>
      <v-col cols="8" md="4">
      <v-card-title>
      <h3> {{text}} <v-icon  large > fas fa-circle-notch fa-spin </v-icon></h3>
      </v-card-title>
      <v-card-text>
    </v-card-text>
    <v-card-actions>
      </v-card-actions>
     </v-col>
      <v-col>
    </v-col>
      </v-row>
     </v-container>
      </v-card>
 </div>`,
  data: () => ({
    text: "В разработке ... ",
    password: "",
    checkbox: "",
  }),
})

export { Develop, UserLogin }

