<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
        'user_id', 'balance',
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }

    public  function scopeBalance_change($query,$user_id,$balance){
        $balance = ($balance == 0 || $balance > 0)?$balance : 0;
        $query->where('user_id','=' ,$user_id)->update(['balance'=>$balance]);

    }
   
}
