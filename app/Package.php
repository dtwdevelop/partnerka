<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Client;
use Illuminate\Support\Carbon;

class Package extends Model
{
    protected $casts = [
        'is_trial' => 'boolean',
        'is_normal' => 'boolean',
        'is_playlist' => 'boolean',
        'is_ministra' => 'boolean',
        'is_iptvportal' => 'boolean',

    ];
    public function countries(){
        
        return $this->belongsToMany(Country::class,'packages_categories');
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function scopeType($query ,$package_id){
       $package = $query->find($package_id);
      
       $date_expire = "";
        switch($package->type){
           case 'hours':  break ;
           case 'days':  $date_expire = Carbon::now()->addDay($package->total_perioad);  break ; 
           case  'months':  $date_expire = Carbon::now()->addMonth($package->total_perioad);  break;
           case 'years': $date_expire = Carbon::now()->addYear($package->total_perioad);    break;
        }
      
       return   $date_expire->toDateTimeString();
    }

}
