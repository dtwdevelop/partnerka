<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Package;
use App\Device;
use App\User;
use Illuminate\Support\Carbon;


class Client extends Model
{
    protected $fillable = ['name','note','email','status','ip','limit' ,'user_id'];
    
    
    public function package(){
        return $this->hasOne(Package::class);
    }

    public function devices(){
        return $this->hasMany(Device::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    

    public  function scopeExpire($query,$id){
        $client = $query->find($id);
        $now = Carbon::now()->toDateString();
        if($now  > $client->expire){
            
            return  true;
        }
        else{
            return false;
        }
 
     }
}
