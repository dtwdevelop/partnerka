<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Client;
use App\Device;
use App\Facades\IptvPortal;

class ApiDeactivator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'device:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    
    public function handle()
    {
       $clients  = Client::all();
       foreach($clients as $client){
        $result = Client::expire($client->id);
        $devices = Device::where('client_id', $client->id)->get();
        foreach($devices as $device){
            if($result){
              IptvPortal::disable_account($device->name,true);
              $this->info("device name {$device->name} : type {$device->type} off");
            }
       }
    }
       return 0;
    }
}
