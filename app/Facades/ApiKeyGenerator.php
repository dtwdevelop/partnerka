<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ApiKeyGenerator extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'apigenerator';
    }
}