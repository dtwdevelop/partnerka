<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ApiV3Service extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'apiv3http';
    }
}