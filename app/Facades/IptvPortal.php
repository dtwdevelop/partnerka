<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class IptvPortal extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'iptvportal';
    }
}