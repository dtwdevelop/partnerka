<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Ministra extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'stalkerapi';
    }
}