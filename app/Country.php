<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Package;

class Country extends Model
{
   
    public function categories(){
        
        return $this->belongsToMany(Category::class,'country_category');
    }

    public function packages(){
        
        return $this->belongsToMany(Package::class,'packages_categories');
    }
}
