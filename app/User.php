<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Balance;
use App\Client;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


    protected static function boot(){
        parent::boot();
        static::deleting(function($user){
            $user->clients()->each(function($client){
                $client->devices()->delete();
              });
              $user->clients()->delete();
              $user->credit()->delete();
          
        });
       
    }

   

    public function credit(){
       return $this->hasOne(Balance::class);

    }

    public function clients(){
        return $this->hasMany(Client::class);
    }
}
