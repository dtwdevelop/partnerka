<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Category;
use Illuminate\Support\Facades\DB;
use Throwable;

class CategoryController extends Controller
{
    //Iptvkey ="aXB0djQ1Njc="
    function category(Request $request){
       $package =  $request->input('id');
        $categories  = Category::all();
       foreach($categories as $category){
        $channels  = $category->channels;
        $playlist = collect();
        foreach($channels as $channel){
           $playlist->push([$category->title => $channel->title]);
 
        }
    }
     
       dd($playlist); 
    }
   /**
    * return all categories
    */
   public  function get_list(Request $request){
       try{
           return Category::all();

       }
       catch(Throwable $err){

       }
   }

   public function category_edit(Request $request){
       try{
           $id  = $request->input('id');
           $category = Category::find($id);
           return response()->json(['category'=>$category,'chanels'=>$category->channels]);
        }
       catch(Throwable $err){
        dd($err);
    }

  }

  public function category_update(Request $request){
      try{
        $this->validate($request,[
            'category.title' => 'required|min:4|max:20',
            'category.description' => 'required| min:4|max:200',
            
        ]);
          $cat  = $request->input('category');
         
           $category = Category::find($cat['id']);
           $category->title = $cat['title'];
           $category->description = $cat['description'];
           $category->save();
           return response()->json(['msg'=>'updated']);
      }
      catch(Throwable $err){
        dd($err);
    }
  }

   public function destroy(Request $request){
       try{
           $id = $request->input('id');
           DB::table('channels_categories')->where('category_id', '=',$id)->delete();
           DB::table('country_category')->where('category_id', '=',$id)->delete();
           Category::where('id',$id)->delete(); 
           return  Category::all();
            }
       catch(Throwable $err){
           dd($err);
       }
   }

   public function add_to_category(Request $request){
       try{
       $params = $request->all();
       $category = Category::find($params['id']);
       foreach($params['select'] as $channel){
        $category->channels()->attach($channel['id']);
       }
       return response()->json(['msg'=>'channel add to category']);
       }
       catch(Throwable $err){
        dd($err);
    }
   }

   public function create(Request $request){
       try{
        $this->validate($request,[
            'category.title' => 'required|min:4|max:20',
            'category.description' => 'required| min:4|max:200',
            
        ]);
           $category_add = $request->input('category');
           $category = new Category;
           $category->title = $category_add['title'];
           $category->description = $category_add['description'];
           $category->expire = "2021-03-07";
           $category->total = 1;
           $category->save();
          foreach($category_add['select'] as $data){
            DB::table('channels_categories')->insert(['category_id'=> $category->id ,'channel_id'=>$data['id']]);
           }
           foreach($category_add['select4'] as $data){
            DB::table('country_category')->insert(['category_id'=> $category->id ,'country_id'=>$data['id']]);
           }
       

       }
       catch(Throwable $err){
          
          return response()->json(['error'=>$err]);
       }
   }
}
