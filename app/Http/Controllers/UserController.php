<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Balance;
use Throwable;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Gate;



class UserController extends Controller
{
   

    public function index(Request $request){
       
       if(Gate::allows('is_admin','admin') || Gate::allows('is_admin','superadmin') ){
        $users  = User::select('id','name','email','note','is_active','role','count')->where('is_active','=',1)->get();
        return $users;
       
       }
      return response()->json(['msg'=>'you do not have permission']);
       
       }

    public function store(Request $request){
        try{
            if(Gate::allows('is_admin','admin') || Gate::allows('is_admin','superadmin')){
            $this->validate($request,[
                'client.name' => 'required|min:4|max:20',
                'client.note' => 'required| min:4|max:200',
                'client.email' => 'required|email',
                'client.password' => 'required|min:4|max:20',
                'client.credit' => 'required',
                'client.count' => 'required',
            ]);
         
            $user_p  = $request->input('client');
            $user = new User;
            $user->name = $user_p['name'];
            $user->email = $user_p['email'];
            $user->password = bcrypt($user_p['password']);
            $user->role = $user_p['role'];
            $user->note = $user_p['note'];
            $user->is_active = true;
            $user->count  = $user_p['count'];
            $user->save();
            $user->credit()->create(['user_id'=>$user->id, 'balance'=>$user_p['credit'] ]);
            return response()->json(['msg'=>'user created']);
            }
            return response()->json(['msg'=>'you do not have permission']);
        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
      
    }

    public function update_license(Request $request){
        try{
         $user = User::find($request->input('id'));
          if($user->count != 0){
             $count   = $user->count - 1;
             $user->count  =  $count;
             $user->save();
          }
          return response()->json(['msg'=> 'lisence count updated']);
        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
    }

    public function update_password(Request $request){
        try{
         $user = User::find($request->input('id'));
         $user->password  = bcrypt($request->input('password'));
         $user->save();
         
          return response()->json(['msg'=> 'password updated']);
        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
    }

    public function destroy(Request $request){
        try{
            if(Gate::allows('is_admin','admin') || Gate::allows('is_admin','superadmin')){
          
            $user = User::find($request->input('id'));
            if($user->role != "superadmin"){
                $user->delete();
                return User::all();
            }
          
            
          
            }
            return response()->json(['msg'=>'you do not have permission']);

        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
    }

    public function login(Request $request)
    {
        $credentials = ['email'=> $request->input('email') , 'password'=> $request->input('password')];
        

        if (! $token = auth('api')->attempt($credentials,true)) {
            return response()->json(['error' => 'Unauthorized','msg'=>'please login in'], 200);
        }

        return $this->respondWithToken($token);
    }

    public function logout(Request $request)
    {
        auth('api')->logout();
        $cookie = Cookie::forget('jwt');

        return response()->json(['message' => 'Successfully logged out'])->withCookie($cookie);
    }

    public function refresh(Request $request)
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
       $user  = auth('api')->user();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' =>$user,
             'balance' => $user->credit->balance
        ])->withCookie('jwt',$token, 60*24);
    }


    public function notifies(Request $request){
        try{
            $user = User::find($request->input('id'));
            $notifications = $user->unreadNotifications;
            return response()->json(['notifies'=> $notifications]);

        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
    }

    public function clear_notifies(Request $request){
        try{
            $user = User::find($request->input('id'));
            $user->notifications()->delete();
            return response()->json(['msg'=> 'notify deleted']);

        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);

        }

    }


    public function change_notice(Request $request){
        try{
            $user = User::find($request->input('id'));
            $user->unreadNotifications()->update(['read_at' => now()]);
            return response()->json(['msg'=> 'notify updated']);


        }
        catch(Throwable $err){
            return response()->json(['err'=> $err->getMessage()]);
        }
    }


}
