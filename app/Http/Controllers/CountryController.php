<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use Throwable;

class CountryController extends Controller
{
    public function get_all(){
        try{
           return Country::all();
        }
        catch(Throwable $err){
          return response()->json(['msg'=>$err->getMessage()]);
        }
    }
}
