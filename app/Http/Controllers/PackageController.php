<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers;
use Throwable;
use App\Device;

use function App\Helpers\create_playlist;

class PackageController extends Controller
{
    
    public function create_package(Request $request){
        try{

            
            $params  = $request->input('package');
           // dd($params);
            $package   = new Package;
            $package->name = $params['name'];
            $package->is_trial = $params['is_trial'];
            $package->is_normal  = $params['is_normal'];
            $package->is_ministra  = $params['is_ministra'];
            $package->is_playlist = $params['is_playlist'];
            $package->is_iptvportal  = $params['is_iptvportal'];
            $package->max_connection  = $params['max_connection'];
            $package->total_period  = $params['duration_time'];
            $package->type   = $params['duration']['val'];
            $package->expire ="2021-03-07";
            $package->credit = $params['credit'];
            $package->save();
            foreach($params['categories'] as $data){
                DB::table('packages_categories')->insert(['country_id'=> $data['id'] ,'package_id'=>$package->id]);
               }

            return  response()->json(['msg'=>'add category']);


        }
        catch(Throwable $err){
            return response()->json(['msg'=>$err->getMessage()]);
        }
    }


    public function get_packages(){
        try{
            $packages = Package::all();
            return $packages;
           
        }
         catch(Throwable $err){
            return response()->json(['msg'=>$err->getMessage()]);
        }
   }

   public function get_country_by_package(Request $request){
       try{
        $countries_collection = collect();
        $package = Package::find($request->input('id'));
        $countries  = $package->countries->where('id');
        foreach($countries as $country){
            $countries_collection->push(['id'=> $country->id]);
          
        }
      
       return response()->json($countries_collection);

       }
       catch(Throwable $err){
        return response()->json(['msg'=>$err->getMessage()]);
       }
   }

   public function  get_package(Request $request){
    try{
        return Package::find($request->input('id'));
    }
     catch(Throwable $err){
        return response()->json(['msg'=>$err->getMessage()]);
    }

   }
   
   public function destroy(Request $request){
       try{
           $id = $request->input('id');
           DB::table('packages_categories')->where('package_id', '=',$id)->delete();
           Package::where('id',$id)->delete(); 
           return Package::all();

       }
       catch(Throwable $err){
        return response()->json(['msg'=>$err->getMessage()]);
    }
   }

   /**
    * generate playlist
    */
   public function generate_playlist(Request $request, $hash, $type){
       try{
           $device = Device::where('token','=',$hash)->first();
           $countries = $device->countries()->get() ;
           $playlist   = collect();
           foreach ($countries as $country){
            foreach($country->categories as $category){
             
                $playlist->push([ $category->title => $category->channels ]);
               }
              }
            
              if(!$playlist->isEmpty()){
                $data = create_playlist($playlist ,"", $type, $hash);
                // header('Content-Type: text/plain');
                //  header  ontent-Disposition: attachment; filename="'.$download.'"');
               
                 return response($data,200)->header('Content-Type','audio/x-mpegurl')
                  ->header('Expires', '-1')
                  ->header('Cache-Control', 'must-revalidate')
                  ->header('Pragma', 'public');

              }
              else{
                  return response()->json([ 'msg'=>'package not exit']);
              }
             
          
          }
       catch(Throwable $err){
        return response()->json(['msg'=>$err->getMessage()]);
    }
   }
}
