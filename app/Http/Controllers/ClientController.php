<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Throwable;
use App\Device;
use Illuminate\Support\Facades\Gate;
use App\Package;
use App\Facades\IptvPortal;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ApiRequest;
use App\Facades\ApiKeyGenerator;

 

class ClientController extends Controller
{
    /**
     * get all user for admin or by id to dealear
     */
    public function clients(Request $request){
        try{
            if(Gate::allows('is_admin','admin') || Gate::allows('is_admin','superadmin') ){
                return Client::all();
            }
            else{
                return Client::where('user_id','=',auth('api')->user()->id)->get();

            }
            
            
        }
        catch(Throwable $err){
           return response()->json(['msg'=>$err->getMessage()]);
        }
    }

    /**
     * create client
     */
    public function store(Request $request){
        try{
           
             $this->validate($request,[
                'client.name' => 'required|min:4|max:20',
                'client.note' => 'required| min:4|max:200',
                'client.email' => 'required| email',
            ]);
            $params  = $request['client'];
            $created_by  = auth('api')->user();
           
            
           
           
            Client::create(['name'=>$params['name'], 'note'=>$params['note'],
            'email'=>$params['email'],'limit'=>3,'ip'=>'127.0.01' ,'status'=>1 ,'user_id'=>$created_by->id ]);
            return response()->json(['msg'=>'client created']);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }

    }

    /**
     * destroy client only if admin or send notify to admin
     */
    public function destroy(Request $request ,Client $client){
        try{
          if(Gate::allows('is_admin','admin') || Gate::allows('is_admin','superadmin')){
                Device::where('client_id',$request->input('id'))->delete();
                Client::where('id',$request->input('id'))->delete();
                 auth('api')->user()->notifications()->where('data->client','=',$request->input('id'))->delete();
               
                return Client::all();
            }
            else{
                // send notify
                  $client  = \App\User::where('role','=','superadmin')->get();
                  Notification::send($client , new ApiRequest(['client_id'=>$request->input('id') ,'status'=>'delete']));
                return response()->json(['msg'=>'send request to admin ']);

            }

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }
    

    /**
     * change note 
     */
    public function change_note(Request $request){
        Client::where('id','=' , $request->input('id'))->update(['note'=>$request->input('note')] );
        return response()->json(['msg'=>'note updated']);
    }


    /**
     * change client status
     */
    public function change_status(Request $request){
        try{
            Client::where('id','=' , $request->input('id'))->update(['status'=>$request->input('status')] );
        return response()->json(['msg'=>'status updated']);
        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }

    /**
     * extend client subscribe
     */

    public function balance_extend(Request $request){
        try{
           
            $client = Client::find($request->input('id'));
            $package  = Package::find($request->input('package_id'));
            if($client->user->credit->balance == 0){
                return response()->json(['msg'=>'problem extend', 'status'=>false]);
            }
            $balance = $client->user->credit->balance - $package->credit ;
            if(!Gate::allows('is_admin','admin')){
                $client->user->credit->balance_change($client->user->id, $balance);
                $expire =  Package::type($request->input('package_id'));
                $client->expire = $expire;
                $client->save();
                foreach ($client->devices as $device){
                    IptvPortal::disable_account($device->name,false);
                }
                return response()->json(['msg'=>'extend add ok' ,'status'=>true]);
             }
             else{
                $expire =  Package::type($request->input('package_id'));
                $client->expire = $expire;
                $client->save();
                foreach ($client->devices as $device){
                    IptvPortal::disable_account($device->name,false);
                }
              
                return response()->json(['msg'=>'extend add ok' ,'status'=>true]);
             }
           
            
        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }

    public function getKey(Request $request){
        try{
           
            $public_key =  $request->input('key');
           
           
            $data = ApiKeyGenerator::generate_key($public_key);
            return response()->json(['msg'=>'key created' , 'key'=>$data]);
          
       }
        catch(Throwable $err){
            return  response()->json(['msg'=>$err->getMessage()]);
        }
    }

}
