<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Facades\Ministra;
use App\Facades\IptvPortal;
use Throwable;
use App\Package;
use App\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ApiRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeviceController extends Controller
{
    /**
     *  create device
     */
    public function store(Request $request){
        try{
       
          $device_p =  $request->input('device');
          $user_p = $request->input('user');
            $device = new Device ;
            $device->client_id = $user_p['id'];
            $device->type= 'playlist';
            $device->token = md5($user_p['name']);
            $device->status = true;
            $device->save();
           
            foreach($request->input('countries') as $country){
                DB::table('country_device')->insert(['country_id'=> $country['id'] ,'device_id'=>$device->id]);
             
            }
            return response()->json(['msg'=>'playlist device add']);
        }
        catch(Throwable $err){
            return response()->json(['error'=>$err->getMessage()]);
        }
    }

    /**
     * delete device playlist
     */
    public function delete_playlist(Request $request){
        try{
            Device::where('id',$request->input('id'))->delete();
            DB::table('country_device')->where('device_id',$request->input('id'));
           return response()->json(['msg'=>'device playlist deleted']);

        }
        catch(Throwable $err){
            return response()->json(['error'=>$err->getMessage()]);
        }
    }
    
    public function devices(Request $request){
        try{
            $client = $request->input('id');
            $all_devices = Device::where('client_id','=',$client)->get();
            
            return $all_devices;

        }
        catch(Throwable $err){
            return response()->json(['error'=>$err->getMessage()]);
        }
    }

    /**
     * add stalker account
     */
    public function add_stalker(Request $request){
        try{
            $randpass = time();
            $device_p =  $request->input('device');
            $user_p = $request->input('user');
              $device = new Device ;
              $device->client_id = $user_p['id'];
              $device->type= 'ministra';
              $device->name = $randpass;
              $device->password = $randpass;
              $device->token = md5($user_p['name']);
              $device->status = true;
              $device->save();

         
            $stalker_data = [
                'login'  => $randpass,
                'full_name' =>  str_replace(" ", "_", $user_p['name']),
                'password' => $randpass,
                'account_number' => $device->id,
                'tariff_plan' => $device_p['stalker_tariff'],
                'status'  => 1,
                'end_date' => '03-08-2021',
            ];
            Ministra::add_account($stalker_data);
         return  response()->json(['msg'=>'stalker created ']);

        }
    
    catch(Throwable $err){
        return response()->json(['err'=>$err->getMessage()]);
    }
      
    }

    public function destroy(Request $request){
        try{
            $id = $request->input('id');
            Ministra::destroy_account('1617356760');

        }
        catch(Throwable $err){
            echo  $err->getMessage(); 
        }

    }
   /**
    * change stalker status
    */
    public function change_status_stalker(Request $request){
        try{
            // params in ?id=1617357543&status=0
            $status  = $request->input('status');
            $client_id  = $request->input('id');
            Ministra::change_status($client_id, $status);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
       
    }

    /**
     * change stalker expire date
     */
    public function change_stalker_expire(Request $request){
        try{

        }
        catch(Throwable $err){

        }

    }

    /**
     * get stalker all tariffs
     */
    public function get_stalker_tariff(Request $request){
        try{
           $data =  collect();
           $response_data = Ministra::get_tariffs();
          
           foreach($response_data->results as $tariff){
               $data->push(['id'=>$tariff->external_id, "name"=>$tariff->name]);
           }
            return  $data;

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }



    public function add_portal(Request $request){
        try{
            $randpass = time();
            $device_p =  $request->input('device');
            $user_p = $request->input('user');
            $expire =  Package::type($device_p['tariff']);
            $user = Client::find($user_p['id']);
            $package_price   = Package::find($device_p['tariff'])->credit;
            $user_balance  = $user->user->credit->balance - $package_price;
           if(!Gate::allows('is_admin','admin')){
           $user->user->credit->balance_change($user->user->id,$user_balance);
           }
            $user->expire = $expire;
            $user->save();

              $device = new Device ;
              $device->client_id = $user_p['id'];
              $device->type= 'portal';
              $device->name = $randpass;
              $device->password = $randpass;
              $device->token = md5($user_p['name']);
              $device->status = true;
              $device->save();

              $data = ['login'=> $randpass , 'password'=>$randpass ,
              'tariff_plan'=>$device_p['iptvportal_tariff'],
              'full_name' =>  $user_p['name'],
              'expire' => $expire
              ];
           $res = IptvPortal::add_account($data);
           return response()->json(['msg'=> 'Tarrifs created' , 'info'=> $res]);
        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }

    public function get_portal_tariffs(Request $request){
        try{
           $packages = collect();
           $tariffs = IptvPortal::get_tariffs();
           
           foreach($tariffs->result as $value){
            $packages->push(['id'=>$value[0] ,'name'=>$value[1]]);
           }
           return $packages;
         
        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }

    }

    /**
     * delete portal
     */
    public function delete_portal(Request $request){
        try{
            $id = $request->input('id');
            Device::where('name',$request->input('id'))->delete();
            IptvPortal::destroy_account($id);
            return response()->json(['msg'=>'portal deleted']);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);

        }
    }

    /**
     * disable portal
     */
    public function disable_portal(Request $request){
        try{
            $id = $request->input('id');
            IptvPortal::disable_account($id ,true);
            Device::where('name', $id)->update(['status'=> false]);
            return response()->json(['msg' =>'portal disabled']);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);

        }

    }


    public function change_portal_tarrifs(Request $request){
        try{
            if($request->has('tariff')){
                $id = $request->input('id');
               $tarrif = $request->input('tariff');
              IptvPortal::change_tarrifs(['id'=>$id, 'tariffs'=> $tarrif]);
               return  response()->json(['msg'=>'Tarrifs updated']);
            }
            else{
                return  response()->json(['msg'=>'tariffs not in request']);
            }
            
         }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);

        }
    }

    public function get_list_subscriber(Request $request){
        try{
           
            $data = IptvPortal::info_debug();
            dd($data);
           return response()->json(['data'=>$data]);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);

        }

    }

    
    public function token_check(Request $request){
        try{
           Log::info($request->input('token'));
            if($request->has('token')){
                $token = $request->input('token');
                $type  = $request->input('type');
                $token_confirm = Device::where('token',$token)->first();
             
                if($token_confirm != null){
                 if($token_confirm->token  == $token){
                   //  dd($token_confirm);
                    return response("",200)->header('HTTP/1.0', '200 OK')
                     ->header('X-UserId', $token_confirm->token)
                     ->header('X-Max-Sessions', '1')
                     ->header('"X-Unique', 'true')
                     ->header('X-AuthDuration', 60);
                     
                     
                    }
     
                }
                
                else{
                return response("",403)->header('HTTP/1.0', '403 Forbidden');
                }

            }
            else{
                return response("",403)->header('HTTP/1.0', '403 Forbidden');
               }
          
         
          }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }

    
}
