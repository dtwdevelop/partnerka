<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use App\Channel;
use App\Facades\ApiService;
use App\Facades\ApiV3Service;
use Throwable;


class ChannelController extends Controller
{
    public function index(Request $request){
        return Channel::all();
    }

   public  function channels(Request $request){
        try{
            if(env('FLUSV3_VERSION') == true){
                ApiV3Service::getChanels();
            }
            else{
                ApiService::getChanels();
            }
           
         return  response()->json(['loading'=>false]);
       }
       catch(Throwable $error){
           dd($error->getMessage());
       }
       
    }

    /** 
     * get channels status
     */
    public function channels_status(Request $request ){
        try{
            $channels_status  =  ApiV3Service::getChanelsLog();
            return  response()->json(['channels'=>$channels_status]);

        }
        catch(Trowable $err){
            return  response()->json(['errors'=>$err->getMessage()]);
        }

    }

    public function api_test(Request $request){
        ApiV3Service::getChanels();
       // $data =  ApiV3Service::get_count_tokens();
       
        return  response()->json(['test'=>"streams"]);

    }

   
    
   
}
