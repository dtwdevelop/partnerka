<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Throwable;
use App\Facades\ApiService;
use App\Facades\ApiV3Service;
use DebugBar\DebugBar;
use Illuminate\Support\Facades\DB;


class SettingController extends Controller
{
    public function get_token_count(Request $request){
        try{
           // dd($request->input('version'));
            if($request->input('version') == "true"){
                $data  =  ApiV3Service::get_count_tokens();
               
                return  response()->json($data);
            }
            else{
                $data  =  ApiService::get_count_tokens();
                return  response()->json($data);
            }
         
          
         //\Debugbar::info("token", $data);

        }
        catch(Throwable $err){
            return response()->json(['err'=>$err->getMessage()]);
        }
    }

    public function store(Requset $request){

    }

    public function settings_save(Request $request){
       try{
           dd($request->input('data'));

        $data  = [
            'flussonic_login' => '',
            'flussonic_password' =>  '',
            'link_api' => '',
            'domain_url' => '',
            'sub_domain' => '',
             'epg_link' =>  '',
             'static_link' => '',
             'stalker_login' => '',
             'stalker_password' =>  '',
             'stalker_link_api' => '',
             'iptvportal_login' =>  '',
             'iptvportal_password' =>  '',
             'iptvportal_link' =>  '',
             'limit_device' =>  '',

        ];
        foreach($data as $key => $value){
            DB::table('settings')->insert(['key'=>$key, 'value'=>$value]);
        }
      
        return response()->json(['msg'=>'setting save']);

       }
       catch(Throwable $err){
        return response()->json(['err'=>$err->getMessage()]);
       }

    }

    public function get_settings(Request $request){
        try{

        }
        catch(Throwable $err){
         return response()->json(['err'=>$err->getMessage()]);
        }


    }


}
