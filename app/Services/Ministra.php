<?php
namespace App\Services;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;


class Ministra {
    private $url = "";
    private $login = "";
    private $password ="";
    private $result ="";
    private $headers  ;

    public function __construct()
    {
        $this->url  = env('STALKER_URL');
        $this->login  = env('STALKER_LOGIN');
        $this->password  = env('STALKER_PASSWORD');
       
        
    }

    public function get_tariffs(){
        
            $auth = [ 'auth' =>[ $this->login, $this->password]];
            $client = new Client([ 'timeout'  => 60]);
            $promise =  $client->getAsync($this->url.'tariffs',$auth);
     
            $promise->then(
                function ( $response) {
                  $res =  $response->getBody() ;
                  $this->headers = $response->getHeaders();
                  if($response->getStatusCode()  == 200){
                    $this->result = json_decode($res);
                    
                   }
                      
                },
                function ( $error) {
                  
                  echo $error->getMessage();
                }
            );
            $promise->wait();
            $data  = $this->result;
            return $data;

    }

    public function add_account($stalker_data){
       
          $params = ['form_params'=>$stalker_data ];
          $header_opt = [ 'auth' =>[ $this->login, $this->password] , 'timeout'  => 60  ];
          $client = new Client($header_opt);
          $promise =  $client->postAsync($this->url.'accounts', $params );
          $promise->then(
                function ( $response) {
                  $res =  $response->getBody() ;
                  $this->headers = $response->getHeaders();
                  if($response->getStatusCode()  == 200){
                    
                    $this->result = json_decode($res);
                    
                   }
                      
                },
                function ( $error) {
                  
                  echo $error->getMessage();
                }
            );
            $promise->wait();
            dd($this->result);
  }

  public function account($id){
       $auth = [ 'auth' =>[ $this->login, $this->password]];
       $client = new Client([ 'timeout'  => 60]);
       $promise =  $client->getAsync($this->url.'users/'.$id,$auth);

       $promise->then(
           function ( $response) {
             $res =  $response->getBody() ;
             $this->headers = $response->getHeaders();
             if($response->getStatusCode()  == 200){
               $this->result = json_decode($res);
               
              }
                 
           },
           function ( $error) {
             
             echo $error->getMessage();
           }
       );
       $promise->wait();
       dd($this->result);
}


    public function change_package(){

    }

    public function destroy_account($account_id){
        $auth = [ 'auth' =>[ $this->login, $this->password]];
        $client = new Client([ 'timeout'  => 60]);
        $promise =  $client->deleteAsync($this->url.'users/'.$account_id,$auth);
        $promise->then(
            function ( $response) {
              $res =  $response->getBody() ;
              $this->headers = $response->getHeaders();
              if($response->getStatusCode()  == 200){
                $this->result = json_decode($res);
                
               }
                  
            },
            function ( $error) {
              
              echo $error->getMessage();
            }
        );
        $promise->wait();
        dd($this->result);

    }

    public function change_status($id, $status){
         
         $params = ['form_params'=>['status'=>$status] ];
         $header_opt = [ 'auth' =>[ $this->login, $this->password] , 'timeout'  => 60  ];
         $client = new Client($header_opt);
         $promise =  $client->putAsync($this->url.'users/'.$id, $params );
         $promise->then(
               function ( $response) {
                 $res =  $response->getBody() ;
                 $this->headers = $response->getHeaders();
                 if($response->getStatusCode()  == 200){
                   
                   $this->result = json_decode($res);
                   
                  }
                     
               },
               function ( $error) {
                 
                 echo $error->getMessage();
               }
           );
           $promise->wait();
           dd($this->result);

    }

    public function change_expire($id,$date,$status=1){
        $params = ['form_params'=>['tariff_expired_date'=>$date, 'status'=>$status] ];
        $header_opt = [ 'auth' =>[ $this->login, $this->password] , 'timeout'  => 60  ];
        $client = new Client($header_opt);
        $promise =  $client->putAsync($this->url.'users/'.$id, $params );
        $promise->then(
              function ( $response) {
                $res =  $response->getBody() ;
                $this->headers = $response->getHeaders();
                if($response->getStatusCode()  == 200){
                  
                  $this->result = json_decode($res);
                  
                 }
                    
              },
              function ( $error) {
                
                echo $error->getMessage();
              }
          );
          $promise->wait();
          dd($this->result);

    }

  
}