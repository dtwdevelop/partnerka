<?php

namespace App\Services;
use GuzzleHttp\Client;
use App\Channel;
use Throwable;
use DebugBar\DebugBar;

class ApiGenerator {

   protected $requestKey = <<<EOD
   -----BEGIN RSA PRIVATE KEY-----
   MIIJKAIBAAKCAgEA4VZur0RkMW3akteITMvErTrGUoMf4se7C/cSEiMC1rVfJB77
   EQghszJUf2Y3hPMUdXev/fFgxG+RVg/vsc5TflUBEU141XVIxYmhNxMW63vYeO4I
   db1aTdDb0ZpCzOE/o26d5UNJai8jen0X83NE5s5nvkj3629FqU6LYKO5NR23fQrd
   b5G9hkShtP5zPDfi0nzLy1zs8yT7BRflI+ZwcS1DAnAvt3aG3oyyPQ35V9IgEqcP
   F7AFMqx8dPoCjxDxPbBLD/Zc7tlbs3he3sDOxgjVTXoQrj9A1jAYJ3G9NfW5Hxpu
   aGMCnhjQtOKaovhSsUm3KkRgDwWbcrL3+8FTLSX8e32zsEERp3B28FiU7/3AckkD
   XGC+ojrkUUnJJjv8p4g+el3rW/+2tZzgPO7hYC9FA3ct0oRtDNinpKVTwE0P1/Rh
   lhJCmJllQ/diRUROPXSNQSJ90IFvaO+cHIgdW8dLWDWO2FAH2DjCwyOMZcX6ELSX
   qjWWQOJZOyztRWgJsj9MWHQRJywx0LzERxtz73hI1l6XuQY2psvxyq59UZKYSuyn
   AEsnTyozNewH0KXzJuCzPSeZ3UDxHRyqB7nEIDkZoe6z8dryQKRL+5hbQ4Jtz4Cj
   k4dtsmRFIaJttLDdu34DhH+95LA7xxv1za32HSKhx3fJ1MrCV7rg5Pj+lh0CAwEA
   AQKCAgABh7G2tPMtNr6+MoJ1awjJ2FVSaBuGUa7edO4Fe4FgbdjYiPFRe0mnStcy
   CkKkP3ZLGeXzBnmOu+czu2X4QhtlBzS7plgEswgsdzPGCPYl4TBmGc+q5P9A+gMU
   GEUN2RWyiWS/Esbj1rAZ+9F20LiODKa9rwcn7Rh+/s6JeHMgfjQpwDIiuM2dpMeo
   T/X+Wa7LmRdVKStet0iRD0l6Be2LhzAmSxa32zEtP61SJs8jnoSjO8kpcwOGBY/O
   x4fO6uGIdrL7x8N3x7gMruuN/acpxxOTtBl65WkOS0NOuUH8pM99+5L2WH3DDw4T
   vFoqcx80nviEa/RvBa25iKeQaQ6kg5MpZwYdtX1oRiIcoioXjHwslAlgRE4GanEN
   qm4ZFehx7os4FJFT89Q4dConc+t1zWUq7t8M7wUIc7csOJ0olbghjOatTIc4t52k
   5O+RMx38TsSnHS8WzcfThrUsBFmtMBQaKJmCvQU3t4X4yUAZwB7lmj2u/CKFvvjg
   tUa+4tKnE22ALBKiGbu8KvEgva2swNDAmXglk56Evsuz7zhWwAwICN6nwlSNfbhl
   Vs8IwCBF7qUBk30jEuxTbtVEmalgZ4qrLms/Q+17jj+DnuB/lZnGAJW8m6Yv0MVk
   csH0H+LT2tyzHzPw3UXAAcMZDpdmHjPc7VmVky2MuD4XrVTQIQKCAQEA90eXp15u
   +W/Xu/hvQmDv8Cqa9cdj2SqSw2QEb7Beovs6eSDuuaBd+WxF9bjDSLHobVC3KCHV
   RRWbI0B7puFN+UnAsYKzE6ygIEosAfiRQxC2NHcU30s/zwLv4ljvAuvgOTIXBh+n
   Y8wgtCv2Pqp2YJnY14gj2gFQiBKE/Mcf95Detb4z9rSl7yfMj+bzjeqT032rK/Qh
   o8hR8U2WtTGk4+AX0UXpozK/pV08Jolmg+qiNMDXNdaqQ/8RIyvBrZ8pk9vP3uhA
   bt0uCTID/MFZKNMqEgWVNlzbuCgYzrE0ouCdpheALvHMw1CjriQ6FQ/33ngiIYjr
   dfWUshKaf2deNQKCAQEA6UjAECNb+C0qSdhh3NU+bT3JZatml4SwRuhdQciE3JVd
   iGmy4jA66gCjNOYc/IcSBCD7K/bQ6RGQV9kaKMymv4PHR1eUo2Ra8I5ufhB059JI
   HHEik565ne0ox9O7UHGawtuQK5j9/hegFJafUa0CQMLAIDQ2f4hZ63ak10tyMy6s
   XzphtwBcNaezPxp9FaOSlrIxZfwofWoYRM+BGxR29ZrQNyvNB0XHKgGpsJGnBUFw
   AkZDJI1f00uV81ZUeE9/CLkXC4WwvxFXy2KHM/tyonV7Ov30UJvKClcC/OzkPupI
   BHmLkArCqUHYWQxb+UivkBxW+eoWVwd/2JccNWz1SQKCAQBTNH/PDXOB8yRAV6P4
   Te3ufieOM/1hAXZI0Mq9Wjc92BMg1vf89f0pBEm2GOl8+IAg6Fv44QmGKXL+NaBl
   0sIN88cm87eWEyv+MXcFYsJeMq1ckgUv7X4XBgH5w7sI3J2iITymDtigN/t5JKHD
   DFCwyJOyUljd7jtOzgx5kfcrsk9+D/YkOTzscBPhB/FD+36qV0rVi1wWZD1OWLEx
   XvcAxlK9VCz9gQIDfo/S18Dq+nSCE7FWqFi6cIUvtHWDGl8/JWOYucWUiFAJQA8f
   aV+64PLzBXXjyPMV0EgopG/1rmbdkGLNaQdhYSzyV7ZDiA/7h6jUZ+Hy2yOhl6js
   3bdRAoIBAQCvPPT5qnYVsWfI3QB2cMFaMkWtV9nTC9uXCEm1drlHMfR22l+DxZMb
   4Kq4u/sePyBrdZL37Yn06jqxouYhq8m+p3D0yHdhXatqdLPPgvEuiymcI+hDV8I7
   PcGaihR1U0FcCq778Qsq6fhYPuUi54F/wEuuZUpi9rwFQrSE/amlhPP6/NIWM/EQ
   ZQ8i4fUAYkVlaintYEPSIfZlCjMDcpYB3O934jE7u1Dm3yTm/CKosgU/0CPzuzc2
   c9sKVdlSHvVgShljkGO6w35dy3+grsvn+bQ+FjKGsuBMcBxQfmreUAMVSXBrHYoo
   tsub6MrWMaGf8YnsZcmuSnXg73JDN7vxAoIBAG+A/akTgMXtnG+0uEntCtUAZRwQ
   l8ztjQ8pwMTIw5gIi2OSl/9sO/DCvJairr2/36bMUTxOnp+mssRQFgd023SKSLNo
   GbPXq2ZxSzJN/o8GH8QFPT8mnRNvgis++8KpW8SPuzD/jRwB2uCmrAqO5uzvx72N
   A61kXuIczUQjh04bg5j5PUcNt8btfC7jbK6c7OcIl9kxM3yI0gNMw61h70zRKR67
   Evt/PqJBmCePxLYHwQBLZzedOZt63+ktplxC/2M/p89JL5Y0nmypbthOnWX0k0DT
   5DD/JYsslR2ulEcZn8uUAcWIxBzj50Ku/sJiUE2WnhnruZzELorDLvoa2tE=
   -----END RSA PRIVATE KEY-----
   EOD;
    private $result;
    



    public function generate_key($param){
        try{
           $info_response =  $this->request_api(env('GENERATOR_INFO'),$param);
          
            $activation_response  = $this->request_api(env('GENERATOR_ACTIVATION'),$param);
            return $this->result->answer;
           
          
        }
        catch(Throwable $err){
            \Debugbar::info("responce from api server", $err->getMessage());
        }
    }

    protected function request_api($url,$param) {
        
       try{
        
        $params = [  'json' => ['offline_request'=>$param, "private_key"=>$this->requestKey ]];
        $header_opt = [ 'timeout'  => 60, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0", 'Content-Type' => 'application/json']];
         $client = new Client($header_opt);
         $promise =  $client->postAsync($url,$params);
        $promise->then(
          function ($response) {
            $res =  $response->getBody();
            $this->headers = $response->getHeaders();
            if ($response->getStatusCode()  == 200) {
    
              $this->result = json_decode($res);
            }
          },
          function ($error) {
    
            echo $error->getMessage();
          }
        );
        $promise->wait();
        
        

       }
     
         catch(Throwable $err){
            dd($err->getMessage());
         }

    }
}