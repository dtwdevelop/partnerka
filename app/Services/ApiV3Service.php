<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Channel;
use Throwable;

class ApiV3Service
{
    protected $total  = 0;
    protected $token = "";
    protected $data;
    protected $url_api ;
    private $channels;

    public function __construct(){
        $this->url_api  =  env("SERVERV3");
    }
   
    /**
     * get channels from remote server 
     */
    public function getChanels()
    {
        try {


            $url  = "{$this->url_api}/flussonic/api/v3/streams";
            $client = new Client(['timeout'  => 6, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUSV3_LOGIN'), env('FLUSV3_PASSWORD')]]);

            $promise->then(
                function ($response) {
                     $channels = $response->getBody();
                    if ($response->getStatusCode()  == 200) {
                        $channels_decoded = json_decode($channels);
                        foreach ($channels_decoded->streams as $key => $channel) {
                            if($channel->stats->alive == "false") continue;        
                            $title  = $this->get_channel($channel->name);
                            $this->insert_channel(["channel" => $channel->name, "title" => $title->title]);
                            sleep(0.5);
                        }
                    }
                },
                function ($error) {
                    dd($error->getMessage());
                }
            );
            $promise->wait();
           
        } catch (Throwable  $err) {
        }
    }

    /**
     * get alive status channels
     */
    public function getChanelsLog($flag="true")
    {
        try {

            $not_work_channels  = collect();
            $url  = "{$this->url_api}/flussonic/api/v3/streams";
            $client = new Client(['timeout'  => 6, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $response =  $client->get($url, ['auth' => [env('FLUSV3_LOGIN'), env('FLUSV3_PASSWORD')]]);
            $channels = $response->getBody();
            if ($response->getStatusCode()  == 200) {
                $channels_decoded = json_decode($channels);
                foreach ($channels_decoded->streams as $key => $channel) {
                  
                  if($channel->stats->alive == $flag) continue; 
                  $title ="";  
                  if(isset($channel->title)) $title = $channel->title ;    
                    $not_work_channels->push(['name'=>$channel->name, 'title'=>$title , 'status'=>$channel->stats->alive]);
                    }
                }
                   
            return $not_work_channels;
        } catch (Trowable $err) {
            dd($err->getMessage());

        }
    }

   

    /**
     * get channel by name from server
     */
    private function get_channel($name)
    {
        try {
            $url  = "{$this->url_api}/flussonic/api/v3/streams/$name";
            $client = new Client(['timeout'  => 6, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUSV3_LOGIN'), env('FLUSV3_PASSWORD')]]);
            $this->data = "";
            $promise->then(
                function ($response) {
                    if ($response->getStatusCode()  == 200) {
                        $channel_decoded = json_decode($response->getBody());
                       
                        $this->data = $channel_decoded;
                    }
                },
                function ($error) {
                    dd($error->getMessage());
                }
            );
            $promise->wait();
            return $this->data;
        } catch (\Exception $error) {
            dd($error->getMessage());
        }
    }

    /**
     * insert channel to db
     */
    private function insert_channel($data)
    {
        try {


            $channel_db = Channel::where('channel', $data['channel'])->first();
            Channel::unguard();
            if ($channel_db == null) {

                Channel::create(["channel" => $data['channel'], "title" => $data['title']]);
            } else {

                if ($channel_db['channel'] !== $data['channel']) {
                    Channel::create(["channel" => $data['channel'], "title" => $data['title']]);
                }
            }
        } catch (Throwable $err) {
            dd($err);
        }
    }

    /**
     * get session token
     */
    public function get_sessions($url)
    {
        try {
           
            $url  = "$url/flussonic/api/v3/sessions";
            $client = new Client(['timeout'  => 120, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUSV3_LOGIN'), env('FLUSV3_PASSWORD')]]);
            $this->data = "";
            $promise->then(
                function ($response) {
                    if ($response->getStatusCode()  == 200) {
                        $sessions_token = json_decode($response->getBody());
                      
                        $this->data = $sessions_token;
                       
                    }
                    
                },
                function ($error) {
                 dd("error");
                }
            );
            $promise->wait();
            return $this->data;
        } catch (Throwable $err) {
            dd($err->getMessage());
        }
       

    }

    /**
     * count total active session
     */
    public function get_count_tokens()
    {
        $servers  = explode(',', env('SERVERV3_LISTS'));
       

        $datas_url  = collect();
        foreach ($servers as $server) {
            $data = $this->get_sessions($server);
            
           
            if($data != null){
                $datas_url->push($data->sessions);
            }
           
        }
       

     //   dd($datas_url);
        $token_list  = collect();
      
        foreach ($datas_url as $item) {
      
            foreach ($item as  $data) {
              
              // if($data == 'user.list') continue;
              
                foreach($data as  $token) {
                   
                    if (isset($data->token)) {
                    
                    $this->token =  $data->token;
                    if ($token_list->where('token', $data->token)->count() !== 0) {
                       
                        $updated =  $token_list->map(function ($item) use ($data) {
                           
                            if ($item['token'] == $this->token) {
                                $total = $item['total'] += 1;
                                return ["token" => $item['token'], "total" => $total, "agent" => $item['agent'], "user_id" => $item['user_id'],"ip"=> $item['ip'], "country" => $item['country']];
                            } else {
                                return ["token" => $item['token'], "total" => $item['total'], "agent" => $item['agent'], "user_id" => $item['user_id'], "ip"=> $item['ip'], "country" => $item['country']];
                            }
                        });
                        $token_list  = $updated;
                    } else {
                        $token_list->push(["token" => $data->token, "total" => 1, "agent" => $data->user_agent, "user_id" => $data->user_id, "ip"=>$data->ip, "country" => $data->country,]);
                    }
                }
            }
            }
        }

        return $token_list->toArray();
    }
}
