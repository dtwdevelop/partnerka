<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Channel;
use Throwable;

class ApiService
{
    protected $total  = 0;
    protected $token = "";
    protected $data;
   
    /**
     * get channels from remote server
     */
    public function getChanels()
    {
        try {


            $url  = "http://51.75.54.161:8080/flussonic/api/media";
            $client = new Client(['timeout'  => 6, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUS_LOGIN'), env('FLUS_PASSWORD')]]);

            $promise->then(
                function ($response) {

                    $channels = $response->getBody();
                    if ($response->getStatusCode()  == 200) {
                        $channels_decoded = json_decode($channels);
                        foreach ($channels_decoded as $key => $channel) {
                            $title  = $this->get_channel($channel->value->name);

                            $this->insert_channel(["channel" => $channel->value->name, "title" => $title->title]);
                            sleep(0.5);
                        }
                        return $this->channels_data;
                    }
                },
                function ($error) {
                    dd($error->getMessage());
                }
            );
            $promise->wait();
        } catch (\Exception $err) {
        }
    }

    /**
     * get channel by name from server
     */
    private function get_channel($name)
    {
        try {
            $url  = "http://51.75.54.161:8080/flussonic/api/media_info/$name";
            $client = new Client(['timeout'  => 6, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUS_LOGIN'), env('FLUS_PASSWORD')]]);
            $this->data = "";
            $promise->then(
                function ($response) {
                    if ($response->getStatusCode()  == 200) {
                        $channel_decoded = json_decode($response->getBody());
                        $this->data = $channel_decoded;
                    }
                },
                function ($error) {
                    dd($error->getMessage());
                }
            );
            $promise->wait();
            return $this->data;
        } catch (\Exception $error) {
            dd($error->getMessage());
        }
    }

    /**
     * insert channel to db
     */
    private function insert_channel($data)
    {
        try {


            $channel_db = Channel::where('channel', $data['channel'])->first();
            Channel::unguard();
            if ($channel_db == null) {

                Channel::create(["channel" => $data['channel'], "title" => $data['title']]);
            } else {

                if ($channel_db['channel'] !== $data['channel']) {
                    Channel::create(["channel" => $data['channel'], "title" => $data['title']]);
                }
            }
        } catch (Throwable $err) {
            dd($err);
        }
    }

    /**
     * get session token
     */
    public function get_sessions($url)
    {
        try {
            $url  = "$url/flussonic/api/sessions";
            $client = new Client(['timeout'  => 120, 'headers' => ['User-Agent' => "User-Agent: Mozilla/5.0",]]);
            $promise =  $client->getAsync($url, ['auth' => [env('FLUS_LOGIN'), env('FLUS_PASSWORD')]]);
            $this->data = "";
            $promise->then(
                function ($response) {
                    if ($response->getStatusCode()  == 200) {
                        $sessions_token = json_decode($response->getBody());
                      
                        $this->data = $sessions_token;
                       
                    }
                    
                },
                function ($error) {
                 dd("error");
                }
            );
            $promise->wait();
            return $this->data;
        } catch (Throwable $err) {
            dd($err->getMessage());
        }
       

    }

    /**
     * count total active session
     */
    public function get_count_tokens()
    {
        $servers  = explode(',', env('SERVER_LISTS'));
       

        $datas_url  = collect();
        foreach ($servers as $server) {
            $data = $this->get_sessions($server);
            if($data != null){
                $datas_url->push($data);
            }
           
        }
      

     //   dd($datas_url);
        $token_list  = collect();
      
        foreach ($datas_url as $item) {
      
            foreach ($item as  $data) {
               if($data == 'user.list') continue;
                foreach($data as  $token) {
                  
                    if (isset($token->token)) {
                    $this->token =  $token->token;
                    if ($token_list->where('token', $token->token)->count() !== 0) {
                       
                        $updated =  $token_list->map(function ($item) use ($token) {
                            if ($item['token'] == $this->token) {
                                $total = $item['total'] += 1;
                                return ["token" => $item['token'], "total" => $total, "agent" => $item['agent'], "user_id" => $item['user_id'],"ip"=> $item['ip'], "country" => $item['country']];
                            } else {
                                return ["token" => $item['token'], "total" => $item['total'], "agent" => $item['agent'], "user_id" => $item['user_id'], "ip"=> $item['ip'], "country" => $item['country']];
                            }
                        });
                        $token_list  = $updated;
                    } else {
                        if(!isset($token->user_agent)) $token->user_agent = "browser";
                        $token_list->push(["token" => $token->token, "total" => 1, "agent" => $token->user_agent, "user_id" => $token->user_id, "ip"=>$token->ip, "country" => $token->country,]);
                    }
                }
            }
            }
        }

        return $token_list->toArray();
    }
}
