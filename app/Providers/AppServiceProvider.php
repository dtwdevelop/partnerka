<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ApiService;
use App\Services\Ministra;
use App\Services\IptvPortal;
use App\Services\ApiGenerator;
use App\Services\ApiV3Service;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('apihttp', function($app){
            return new ApiService();
         });
         
         $this->app->singleton('apiv3http', function($app){
            return new ApiV3Service();
         });
         
         $this->app->singleton('stalkerapi', function($app){
            return new Ministra();
         });
         $this->app->singleton('iptvportal', function($app){
            return new IptvPortal();
         });
         
         $this->app->singleton('apigenerator', function($app){
            return new ApiGenerator();
         });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
