<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Client;


class Device extends Model
{
    
    protected $fillable = [
        'status',
    ];

    public function countries(){
        
        return $this->belongsToMany(Country::class,'country_device');
    }

    public function client(){
        return $this->hasOne(Client::class);
    }

   

}
