<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Channel;
use App\Country;

class Category extends Model
{
    //
    public function channels(){
        
        return $this->belongsToMany(Channel::class,'channels_categories');
    }

    public function packages(){
        
        return $this->belongsToMany(Country::class,'country_category');
    }
}
